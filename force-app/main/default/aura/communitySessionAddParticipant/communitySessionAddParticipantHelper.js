({
	showSuccess : function(component, event) {
		var toastEvent = $A.get("e.force:showToast");
		toastEvent.setParams({
			"title": "Success!",
			"type" : 'success',
			"mode" : 'dismissible',
			"duration" : 5000,
			"message": "Participant has been added."
		});
		toastEvent.fire();
	},
	showError : function(component, event) {
		var toastEvent = $A.get("e.force:showToast");
		toastEvent.setParams({
			"title": "Error!",
			"type" : 'error',
			"mode" : 'dismissible',
			"duration" : 5000,
			"message": "Sorry! something went wrong, please contact the administrator."
		});
		toastEvent.fire();
	},
	showErrorBlank : function(component, event) {
		var toastEvent = $A.get("e.force:showToast");
		toastEvent.setParams({
			"title": "Error!",
			"type" : 'error',
			"mode" : 'dismissible',
			"duration" : 5000,
			"message": "Please complete all required fields."
		});
		toastEvent.fire();
	},
     
    // function automatic called by aura:waiting event  
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for displaying loading spinner 
        component.set("v.spinner", true); 
    },
     
    // function automatic called by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hiding loading spinner    
        component.set("v.spinner", false);
    },

    // Created [Namespace Issue Fixes] JaysonLabnao Aug102022
	cleanUpNamespace : function(jsonData){
        let responseStr = JSON.stringify(jsonData);
 
        if(responseStr.includes('beta_ccn__')){
            responseStr = responseStr.replaceAll('beta_ccn__', '');
        }
        if(responseStr.includes('compass_cn__') ){
            responseStr = responseStr.replaceAll('compass_cn__', '');
        }
 
        return JSON.parse(responseStr);
    },

    // Added By JaysonLabnao [CCN-EVE-2160-DV] OCT092022
	getRankOptions : function(component, event, helper){
		const action = component.get('c.getRankAndRankGroupDependencies');
		action.setCallback(this, (response) =>{
			const STATE = response.getState();
			if(STATE === 'SUCCESS'){
				
				//let rankOptions = response.getReturnValue();
				//component.set('v.rankOptions', rankOptions);

				//XEN REYES March 5, 2023 CCN-EVE-2631-DV
				var result = response.getReturnValue();
                var arrayMapKeys = [];
                for(var key in result){
                    arrayMapKeys.push({
						key: key, 
						value: result[key]});
                }
                component.set("v.dataCollectionRecords", arrayMapKeys);

			}
			else{
				console.error({ error : response.getError() });
			}
			component.set('v.spinner', false);
		});

		$A.enqueueAction(action);
	}
})