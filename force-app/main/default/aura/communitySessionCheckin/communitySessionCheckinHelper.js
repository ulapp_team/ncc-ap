({
    ReInit : function(component, event, helper) {
        var url_string = document.location.href;
        var sessionId;
        try {
            sessionId = (url_string.split('sessionid=')[1]).slice(0,15);
        }
        catch(err) {
            console.error('ID Error');
        }
        //var sessionId = (url_string.split('sessionid=')[1]).slice(0,15);
        var action = component.get("c.getSessionParticipants");
 
        action.setParams({ 
            sessionId : sessionId
        });
		action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                // Updated [Namespace Issue Fixes] JaysonLabnao Aug092022
                var resultEvent = helper.cleanUpNamespace(response.getReturnValue());
				component.set('v.participantList', resultEvent);
             }
        });
        $A.enqueueAction(action);
	},
    getSession : function(component, event, helper) {
        var url_string = document.location.href;
        var sessionId;
        try {
            sessionId = (url_string.split('sessionid=')[1]).slice(0,15);
        }
        catch(err) {
            console.error('ID Error');
        }
        //var sessionId = (url_string.split('sessionid=')[1]).slice(0,15);
        var action = component.get("c.getSessionDetails");
 
        action.setParams({ 
            sessionId : sessionId
        });
		action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                // Updated [Namespace Issue Fixes] JaysonLabnao Aug092022
                var resultEvent = helper.cleanUpNamespace(response.getReturnValue());
				component.set('v.name', resultEvent.Name);
                component.set('v.startDate', resultEvent.Start_Date_Time2__c);
                component.set('v.endDate', resultEvent.End_Date_Time2__c);
                
             }
        });
        $A.enqueueAction(action);
	},
    // function automatic called by aura:waiting event  
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for displaying loading spinner 
        component.set("v.spinner", true); 
    },
        
    // function automatic called by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hiding loading spinner    
        component.set("v.spinner", false);
    },

    // Created [Namespace Issue Fixes] JaysonLabnao Aug092022
    cleanUpNamespace : function(jsonData){
        let responseStr = JSON.stringify(jsonData);

        if(responseStr.includes('beta_ccn__')){
            responseStr = responseStr.replaceAll('beta_ccn__', '');
        }
        if(responseStr.includes('compass_cn__') ){
            responseStr = responseStr.replaceAll('compass_cn__', '');
        }
 
        return JSON.parse(responseStr);
    },


})