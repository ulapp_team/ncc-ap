({
    getUserJourney: function (component, journeyId, helper) {
        console.log('---- getUserJourney');
        let action = component.get("c.getUserJourney");
        action.setParams({
            strJourneyId: journeyId
        });
        
        action.setCallback(this, function (response) {
            console.log('---- getUserJourney - setCallback');
            let state = response.getState();
            console.log("!@#STATE: " + state);
            if (state === "SUCCESS") {
                let userJourneyWrapper = this.cleanUpNamespace(response.getReturnValue()); // Start CCN-App Exchange-3211-QA-TE-SMOKE-37 7/11/2026 Von Pernicia: Fixed namespace issue
                console.log("!@#urlFromJourney: " + userJourneyWrapper.urlFromJourney);
                if (userJourneyWrapper.urlFromJourney === false) {
                    component.set("v.userJourney", userJourneyWrapper.userJourney);
                    component.set("v.urlFromJourney", userJourneyWrapper.urlFromJourney);
                    console.log("!@#$ DATA " + component.get("v.userJourney"));
                    if (userJourneyWrapper.lstUserMilestones !== undefined) {
                        component.set(
                            "v.userMilestones",
                            userJourneyWrapper.lstUserMilestones
                        );
                        if (userJourneyWrapper.mapTasksByMilestoneName !== undefined) {
                            var acts = [];
                            for (var key in userJourneyWrapper.mapTasksByMilestoneName) {
                                acts.push({
                                    value: userJourneyWrapper.mapTasksByMilestoneName[key],
                                    key: key
                                });
                                console.log("!@# Milestone: " + key);
                            }
                            component.set("v.userMilestoneRelatedInfo", acts);
                        }
                    }
                } else if (userJourneyWrapper.urlFromJourney === true) {
                    component.set(
                        "v.journeyMilestoneList",
                        userJourneyWrapper.journeyMilestoneList
                    );
                }
                component.set("v.journeyData", userJourneyWrapper.journeyData);
            } else {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        // log the error passed in to AuraHandledException
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    // Start CCN-App Exchange-3211-QA-TE-SMOKE-37 7/11/2026 Von Pernicia: Added cleanupname space and used Org_Namespace custom label to make it configurable
    cleanUpNamespace : function(jsonData){
        let responseStr = JSON.stringify(jsonData);	
        var namespace = $A.get("$Label.c.Org_Namespace");	
        if(responseStr.includes(namespace)){	
            responseStr = responseStr.replaceAll(namespace, '');	
        }	
        	
        return JSON.parse(responseStr);	
    }
    // End CCN-App Exchange-3211-QA-TE-SMOKE-37 7/11/2026 Von Pernicia: Added cleanupname space and used Org_Namespace custom label to make it configurable
});