({
    getUserJourney: function (component, journeyId) {
        let milestoneList = [];
        var userJourney = component.get("c.getUserJourney");
        userJourney.setParams({
            strJourneyId: journeyId
        });
        
        userJourney.setCallback(this, function (response) {
            var state = response.getState();
            console.log('state ::: ', state);
            
            var currentDateTime = new Date();
            
            if (state === "SUCCESS") {
                var userJourneyWrapper = this.cleanUpNamespace(response.getReturnValue()); // Start CCN-App Exchange-3211-QA-TE-SMOKE-37 7/11/2026 Von Pernicia: Fixed namespace issue
                console.log(userJourneyWrapper);
                //console.log("!@#userJourneyWrapper.urlFromJourney ::: " + JSON.stringify(userJourneyWrapper.urlFromJourney));
                if (userJourneyWrapper.urlFromJourney === false) {
                    component.set("v.userJourney", userJourneyWrapper.userJourney);
                    //console.log("!@#userJourney: " + JSON.stringify(userJourneyWrapper.userJourney));
                    component.set("v.urlFromJourney", userJourneyWrapper.urlFromJourney);
                    //console.log("!@#userJourneyWrapper.lstUserMilestones ::: " + JSON.stringify(userJourneyWrapper.lstUserMilestones));
                    try{
                    for (var key of userJourneyWrapper.lstUserMilestones) {
                        console.log(key);
                        if(key.Milestone__r.Parent_Milestone__c){
                            console.log('key');
                            let parentMilestone = milestoneList[key.Milestone__r.Parent_Milestone__c];
                            let childMilestone = parentMilestone.childMilestone ? parentMilestone.childMilestone : [];
                            childMilestone.push(key);
                            parentMilestone.childMilestone = childMilestone;
                        }else{
                            milestoneList[key.Milestone__c] = key;
                            if(key.Milestone__r.Session_Catch_Up_Start_Date__c != undefined && key.Milestone__r.Session_Catch_Up_End_Date__c != undefined){
                                let catchupDateStart = new Date(key.Milestone__r.Session_Catch_Up_Start_Date__c);
                                let catchupDateEnd = new Date(key.Milestone__r.Session_Catch_Up_End_Date__c);

                                //ADDITIONAL CHECK TO SHOW THE CATCH UP BUTTON
                                // Updated by Xen [CCN843] March252022
                                //var attendanceCompleted = true;
                                //if(key.Name in (userJourneyWrapper.mapMetricsByMilestoneName)){
                                //    for (let key2 of (userJourneyWrapper.mapMetricsByMilestoneName)[key.Name]) {
                                        //if(key2.Metric_Used__c == 'Attendance Date' && (key2.Completion_Date__c == null || key2.Completion_Date__c == undefined)) attendanceCompleted = true;
                                //    }
                                //}
                                // Updated by Xen [CCN843] March252022

                                key.Milestone__r.isCatchupAvailable = key.Milestone__r.Catch_Up_Completion_Button__c && this.computeForSessionCatchUp(component, catchupDateStart, catchupDateEnd, currentDateTime);
                            }
                        }
                        
                    }
                }catch(error){
                    console.error(error);
                }
                    console.log('milestoneList');
                    console.log(milestoneList);
                    if (userJourneyWrapper.lstUserMilestones !== undefined) {
                        component.set(
                            "v.userMilestones",
                            Object.values(milestoneList)
                        );
                        console.log("!@#userJourneyWrapper.mapTasksByMilestoneName ::: " + JSON.stringify(userJourneyWrapper.userJourneyWrapper));
                        if (userJourneyWrapper.mapTasksByMilestoneName !== undefined) {
                            var acts = [];
                            for (var key in userJourneyWrapper.mapTasksByMilestoneName) {
                                acts.push({
                                    value: userJourneyWrapper.mapTasksByMilestoneName[key],
                                    key: key
                                });
                                console.log("!@# Milestone: " + key);
                            }
                            console.log('acts ::: ', acts);
                            component.set("v.userMilestoneRelatedInfo", acts);
                        }
                    }
                } else if (userJourneyWrapper.urlFromJourney === true) {
                    component.set(
                        "v.journeyMilestoneList",
                        userJourneyWrapper.lstUserMilestones
                    );
                    console.log("!@#userJourneyWrapper.journeyData ::: " + JSON.stringify(userJourneyWrapper.journeyData));
                    component.set("v.journeyData", userJourneyWrapper.journeyData);
                }
            } else {
                console.log("!@# User Journey not found!");
            }
        });
        $A.enqueueAction(userJourney);
    },
    
    updateParticipantMilestoneMetric : function(component, event, helper){
        var updateParticipantMilestoneMetric = component.get("c.processSurveyAssessmentComplete");
        const milestoneId = event.getSource().get("v.name");
        updateParticipantMilestoneMetric.setParams({
            participantMilestoneId: milestoneId
        });
        
        updateParticipantMilestoneMetric.setCallback(this, function (response) {
            const state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.spinner", false);
                const hasErrors = response.getReturnValue();
                let message = "We have received your response. Thank you!";
                let type = "Success";
                if(!hasErrors){
                    let userMilestone = component.get("v.userMilestones");
                    for(let i = 0; i < userMilestone.length; i++){
                        if(userMilestone[i].Id === milestoneId){
                            userMilestone[i].Progress__c = 100;
                        }
                    }
                    window.location.reload();
                    component.set("v.userMilestones", userMilestone);
                }else{
                    message = "Something went wrong. Please try again later."
                    type = "Error";
                }
                
                var resultToast = $A.get("e.force:showToast");
                resultToast.setParams({
                    message: message,
                    type: type
                });
                resultToast.fire();
            }
        });
        
        $A.enqueueAction(updateParticipantMilestoneMetric);
    },
    
    updateParticipantEventMetric : function(component, event, helper){
        var updateParticipantEventMetric = component.get("c.processEventCatchUp"); 
        const milestoneId = event.getSource().get("v.name");
        console.log("handleEventCatchUp");
        console.log(milestoneId);
        updateParticipantEventMetric.setParams({
            participantMilestoneId: milestoneId
        });
        
        updateParticipantEventMetric.setCallback(this, function (response) {
            const state = response.getState();
            console.log(state);
            if (state === "SUCCESS") {
                component.set("v.spinner", false);
                const hasErrors = response.getReturnValue();
                let message = "Thank you for completing the Catch-Up steps!";
                let type = "Success";
                if(!hasErrors){
                    let userMilestone = component.get("v.userMilestones");
                    for(let i = 0; i < userMilestone.length; i++){
                        if(userMilestone[i].Id === milestoneId){
                            userMilestone[i].Progress__c = 100;
                        }
                    }
                    //window.location.reload();
                    component.set("v.userMilestones", userMilestone);
                }else{
                    message = "Something went wrong. Please try again later."
                    type = "Error";
                }
                
                var resultToast = $A.get("e.force:showToast");
                resultToast.setParams({
                    message: message,
                    type: type
                });
                resultToast.fire();
            }else if (state === "ERROR") {
                console.log(state);
                let errors = response.getError();
                console.log(errors);
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                    // Updated by RiannoRizarri [CCN843] March252022
                    var resultToast = $A.get("e.force:showToast");
                    resultToast.setParams({
                        message: errors[0].message,
                        type: "Error"
                    });
                    resultToast.fire();
                    // Updated by RiannoRizarri [CCN843] March252022
                }
            }
        });
        
        $A.enqueueAction(updateParticipantEventMetric);
    },
    
    computeForSessionCatchUp : function (component, startDateTime, endDateTime, currentDateTime){
        let isCatchupAvailable = false;
        
        startDateTime.setMinutes(startDateTime.getMinutes() - 15);
        endDateTime.setMinutes(endDateTime.getMinutes() + 30);
        
        if (currentDateTime >= startDateTime && currentDateTime <= endDateTime){
            isCatchupAvailable = true;
        }
        
        return isCatchupAvailable;
    },
    
     // Start CCN-App Exchange-3211-QA-TE-SMOKE-37 7/11/2026 Von Pernicia: Added cleanupname space and used Org_Namespace custom label to make it configurable
    // Created [Namespace Issue Fixes] JaysonLabnao Aug052022
    cleanUpNamespace : function(jsonData){
        let responseStr = JSON.stringify(jsonData);	
        var namespace = $A.get("$Label.c.Org_Namespace");	
        if(responseStr.includes(namespace)){	
            responseStr = responseStr.replaceAll(namespace, '');	
        }	
        	
        return JSON.parse(responseStr);	
    }
    // End CCN-App Exchange-3211-QA-TE-SMOKE-37 7/11/2026 Von Pernicia: Added cleanupname space and used Org_Namespace custom label to make it configurable
});