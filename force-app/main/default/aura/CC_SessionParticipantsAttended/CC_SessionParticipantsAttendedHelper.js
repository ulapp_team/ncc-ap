({
    showToast : function(component, event, message, title,type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type": type
        });
        toastEvent.fire();
    }, 
    
    getPicklistValues : function (component, fieldName){
            
        var action = component.get("c.getPickListValues");
        action.setParams({ fieldName : fieldName});
        
        action.setCallback(this,function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                if (fieldName == 'Readiness_Self_Ranking__c'){
                    component.set("v.readinessPicklistValues",response.getReturnValue());
                }
                if (fieldName == 'Session_Participation__c'){
                    component.set("v.sessionPicklistValues",response.getReturnValue());
                }
            }
            console.log(response.getReturnValue());
        });
        $A.enqueueAction(action);
    },
    
    getAttendedJSON : function(component){
        var recId = component.get("v.recordId");
        var action = component.get("c.getEventAttendedParticipantFieldsJSON");
        action.setParams({sessionId : recId});
        action.setCallback(this,function(response){
            try{
                var state = response.getState();
                if(state === "SUCCESS"){
                    var jsonResponse = JSON.parse(response.getReturnValue());
                    component.set("v.showReadinessSelfRanking", 	jsonResponse["Readiness_Self_Ranking-Session_Participant__c|Readiness_Self_Ranking__c"]!=undefined);
                    component.set("v.showSessionParticipation", 	jsonResponse["Session_Participation-Session_Participant__c|Session_Participation__c"]!=undefined);
                    component.set("v.requireReadinessSelfRanking", 	jsonResponse["Readiness_Self_Ranking-Session_Participant__c|Readiness_Self_Ranking__c"]==true);
                    component.set("v.requireSessionParticipation", 	jsonResponse["Session_Participation-Session_Participant__c|Session_Participation__c"]==true);
                    console.log("show RSR: "+component.get("v.showReadinessSelfRanking"));
                    console.log("require RSR: "+component.get("v.requireReadinessSelfRanking"));
                    console.log("show SP: "+component.get("v.showSessionParticipation"));
                    console.log("require SP: "+component.get("v.requireSessionParticipation"));
                }
                else{
                	console.log('getAttendedJSON failed');
                }
            }
            catch(e){
                console.log(e);
            }            
        });
        $A.enqueueAction(action);
    }
    
    /**
    getAttendedFields : function (component){
        var action = component.get("c.getAttendedFields");
        action.setParams({ recordId : component.get("v.recordId")});
        
        action.setCallback(this,function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                //
                
            }
        });
        $A.enqueueAction(action);
    }
    **/
})