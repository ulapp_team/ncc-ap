trigger TeleMeetTrigger on TeleMeet__c(before insert, after insert, before update, after update, before delete, after delete, after undelete) {


    if (Trigger.isAfter && Trigger.isInsert) {
        
        //Navigator
        Map < Id, Id > conAvaEncounterMap = new Map < Id, Id > ();
        List < Id > conAvaIdList = new List < Id > ();
        Map<ID,Schema.RecordTypeInfo> rt_Map = Telemeet__c.sObjectType.getDescribe().getRecordTypeInfosById();
        for (Telemeet__c t: Trigger.new) {
        
            if(rt_map.get(t.recordTypeID).getName()  == 'Default'){
            
            //Navigator
            conAvaIdList.add(t.ContactAvailabilityId__c);
            conAvaEncounterMap.put(t.ContactAvailabilityId__c, t.Id);
            }
        }

        //Navigator
        List < Contact_Availability__c > conAvaList = [Select Id, Start_Date_Time__c, End_Date_Time__c, Time_zone__c From Contact_Availability__c Where Id IN: conAvaIdList];
        Map < Id, Contact_Availability__c > encounterConAvaRecMap = new Map < Id, Contact_Availability__c > ();
        if (!conAvaList.isEmpty()) {
            for (Contact_Availability__c c: conAvaList) {
                encounterConAvaRecMap.put(c.Id, c);
                c.TeleMeet__c = (conAvaEncounterMap.containsKey(c.Id) ? conAvaEncounterMap.get(c.Id) : null);
            }
            update conAvaList;
        }
    }
    if (Trigger.isBefore && Trigger.isInsert) {

        //Navigator
        Map < Id, Id > conAvaEncounterMap = new Map < Id, Id > ();
        List < Id > conAvaIdList = new List < Id > ();

        for (Telemeet__c t: Trigger.new) {
            //Navigator
            conAvaIdList.add(t.ContactAvailabilityId__c);
            conAvaEncounterMap.put(t.ContactAvailabilityId__c, t.Id);
        }

        //Navigator
        List < Contact_Availability__c > conAvaList = [Select Id, Start_Date_Time__c, End_Date_Time__c, Time_zone__c From Contact_Availability__c Where Id IN: conAvaIdList];
        Map < Id, Contact_Availability__c > encounterConAvaRecMap = new Map < Id, Contact_Availability__c > ();
        for (Contact_Availability__c c: conAvaList) {
            encounterConAvaRecMap.put(c.Id, c);
        }

        //Timezone
        Map < String, String > SFTimezone = new Map < String, String > ();
        for (SFTimezone__mdt s: [Select Id, MasterLabel, DeveloperName, Timezone_Name__c From SFTimezone__mdt]) {
            SFTimezone.put(s.MasterLabel, s.Timezone_Name__c);
        }

        String TimeZoneSidKey = [SELECT Id, TimeZoneSidKey FROM Organization].TimeZoneSidKey;
        Timezone tz;
        String timeZoneUpdate = '';

        for (Telemeet__c tel: Trigger.new) {
            //Timezone Convert
            //Convert Based on the Company Timezone
            DateTime startDT = (encounterConAvaRecMap.containsKey(tel.ContactAvailabilityId__c) ? encounterConAvaRecMap.get(tel.ContactAvailabilityId__c).Start_Date_Time__c : null);
            DateTime endDT = (encounterConAvaRecMap.containsKey(tel.ContactAvailabilityId__c) ? encounterConAvaRecMap.get(tel.ContactAvailabilityId__c).End_Date_Time__c : null);
            
            String startDTGMT = startDT.format('MM/dd/yyyy/HH/mm/ss', TimeZoneSidKey);
            String endDTGMT = endDT.format('MM/dd/yyyy/HH/mm/ss', TimeZoneSidKey);

            system.debug('startDTGMT >>>' + startDTGMT);
            system.debug('endDTGMT>>>' + endDTGMT);

            List < String > startDTtz1 = startDTGMT.split('/');
            List < String > endDTtz1 = endDTGMT.split('/');

            DateTime startDTNewComp = DateTime.newInstanceGMT(Integer.valueOf(startDTtz1[2]), Integer.valueOf(startDTtz1[0]), Integer.valueOf(startDTtz1[1]), Integer.valueOf(startDTtz1[3]),
                Integer.valueOf(startDTtz1[4]), Integer.valueOf(startDTtz1[5]));
            DateTime endDTNewRecComp = DateTime.newInstanceGMT(Integer.valueOf(endDTtz1[2]), Integer.valueOf(endDTtz1[0]), Integer.valueOf(endDTtz1[1]), Integer.valueOf(endDTtz1[3]),
                Integer.valueOf(endDTtz1[4]), Integer.valueOf(endDTtz1[5]));

            //Convert based on the record timezone
            timeZoneUpdate = SFTimezone.get((encounterConAvaRecMap.containsKey(tel.ContactAvailabilityId__c) ? encounterConAvaRecMap.get(tel.ContactAvailabilityId__c).Time_Zone__c : null));
            String startDTGMTRec = startDT.format('MM/dd/yyyy/HH/mm/ss', timeZoneUpdate);
            String endDTGMTRec = endDT.format('MM/dd/yyyy/HH/mm/ss', timeZoneUpdate);

            List < String > startDTtz = startDTGMTRec.split('/');
            List < String > endDTtz = endDTGMTRec.split('/');

            DateTime startDTNewRec = DateTime.newInstanceGMT(Integer.valueOf(startDTtz[2]), Integer.valueOf(startDTtz[0]), Integer.valueOf(startDTtz[1]), Integer.valueOf(startDTtz[3]),
                Integer.valueOf(startDTtz[4]), Integer.valueOf(startDTtz[5]));
            DateTime endDTNewRec = DateTime.newInstanceGMT(Integer.valueOf(endDTtz[2]), Integer.valueOf(endDTtz[0]), Integer.valueOf(endDTtz[1]), Integer.valueOf(endDTtz[3]),
                Integer.valueOf(endDTtz[4]), Integer.valueOf(endDTtz[5]));

            //Convert based on the selected timezone
            timeZoneUpdate = SFTimezone.get(tel.Time_Zone__c);
            String startDTGMTSel = startDT.format('MM/dd/yyyy/HH/mm/ss', timeZoneUpdate);
            String endDTGMTSel = endDT.format('MM/dd/yyyy/HH/mm/ss', timeZoneUpdate);

            List < String > startDTtz2 = startDTGMTSel.split('/');
            List < String > endDTtz2 = endDTGMTSel.split('/');

            DateTime startDTNewSel = DateTime.newInstanceGMT(Integer.valueOf(startDTtz2[2]), Integer.valueOf(startDTtz2[0]), Integer.valueOf(startDTtz2[1]), Integer.valueOf(startDTtz2[3]),
                Integer.valueOf(startDTtz2[4]), Integer.valueOf(startDTtz2[5]));
            DateTime endDTNewSel = DateTime.newInstanceGMT(Integer.valueOf(endDTtz2[2]), Integer.valueOf(endDTtz2[0]), Integer.valueOf(endDTtz2[1]), Integer.valueOf(endDTtz2[3]),
                Integer.valueOf(endDTtz2[4]), Integer.valueOf(endDTtz2[5]));

            //StartDate
            Long dt1Long = startDTNewRec.getTime();
            Long dt2Long = startDTNewSel.getTime();
            Long milliseconds = dt2Long - dt1Long;
            Long seconds = milliseconds / 1000;
            Long minutes = seconds / 60;
            Long hours = minutes / 60;
            Long days = hours / 24;

            DateTime finalStartDate = startDTNewComp.addHours(Integer.valueOf(hours));


            //EndDate
            Long dt1Long2 = endDTNewRec.getTime();
            Long dt2Long2 = endDTNewSel.getTime();
            Long milliseconds2 = dt2Long2 - dt1Long2;
            Long seconds2 = milliseconds2 / 1000;
            Long minutes2 = seconds2 / 60;
            Long hours2 = minutes2 / 60;
            Long days2 = hours2 / 24;

            DateTime finalEndDate = endDTNewRecComp.addHours(Integer.valueOf(hours2));

            //Convert Based on the Company Timezone
            DateTime startDTFinal = finalStartDate;
            DateTime endDTFinal = finalEndDate;

            String startDTGMTFinal = startDTFinal.format('MM/dd/yyyy/HH/mm/ss', TimeZoneSidKey);
            String endDTGMTFinal = endDTFinal.format('MM/dd/yyyy/HH/mm/ss', TimeZoneSidKey);


            List < String > startDTtzFinal = startDTGMTFinal.split('/');
            List < String > endDTtzFinal = endDTGMTFinal.split('/');

            DateTime startDTNewFinal = DateTime.newInstanceGMT(Integer.valueOf(startDTtzFinal[2]), Integer.valueOf(startDTtzFinal[0]), Integer.valueOf(startDTtzFinal[1]), Integer.valueOf(startDTtzFinal[3]),
                Integer.valueOf(startDTtzFinal[4]), Integer.valueOf(startDTtzFinal[5]));
            DateTime endDTNewRecFinal = DateTime.newInstanceGMT(Integer.valueOf(endDTtzFinal[2]), Integer.valueOf(endDTtzFinal[0]), Integer.valueOf(endDTtzFinal[1]), Integer.valueOf(endDTtzFinal[3]),
                Integer.valueOf(endDTtzFinal[4]), Integer.valueOf(endDTtzFinal[5]));

            tel.Start_Date_Time__c = (encounterConAvaRecMap.containsKey(tel.ContactAvailabilityId__c) ? encounterConAvaRecMap.get(tel.ContactAvailabilityId__c).Start_Date_Time__c : null);
            tel.End_Date_Time__c = (encounterConAvaRecMap.containsKey(tel.ContactAvailabilityId__c) ? encounterConAvaRecMap.get(tel.ContactAvailabilityId__c).End_Date_Time__c : null);
            tel.Time_Zone__c = (encounterConAvaRecMap.containsKey(tel.ContactAvailabilityId__c) ? encounterConAvaRecMap.get(tel.ContactAvailabilityId__c).Time_zone__c : null);

        }
    }

    if (Trigger.isAfter && Trigger.isInsert) {
        List < TeleMeet_Participant__c > conPar = new List < TeleMeet_Participant__c > ();

        //List<Telehealth_Consultation_Group__c> tcg = new List<Telehealth_Consultation_Group__c>();
        List < Id > pracManaIds = new List < Id > ();
        Map < Id, Id > connectPractMana = new Map < Id, Id > ();
        Set<Id> connectNavAgent = new Set<Id> ();


        for (Telemeet__c t: Trigger.new) {
            if (t.Practice_Management__c != null) {
                pracManaIds.add(t.Practice_Management__c);
                connectPractMana.put(t.Practice_Management__c, t.Id);
            }
            connectNavAgent.add(t.Resource_Contact__c);
        }



        List < Telehealth_Consultation_Group__c > tcg = [Select Id, Contact__c, Practice_Management__c, Role__c From Telehealth_Consultation_Group__c Where Practice_Management__c In: pracManaIds];

        if (!tcg.isEmpty()) {
            for (Telehealth_Consultation_Group__c t: tcg) {
                TeleMeet_Participant__c patient = new TeleMeet_Participant__c();
                patient.Contact__c = t.Contact__c;
                patient.Role__c = t.Role__c;
                patient.TeleMeet__c = (connectPractMana.containsKey(t.Practice_Management__c) ? connectPractMana.get(t.Practice_Management__c) : null);
                conPar.add(patient);
            }
        }

        //for Navigator force update of Connect Status to Scheduled 
        List < Telemeet__c > connectToUpdate = new List < Telemeet__c > ();
        for (Telemeet__c t: Trigger.new) {
            if (t.Resource_Contact__c != null) {
                TeleMeet_Participant__c provider = new TeleMeet_Participant__c();
                provider.Contact__c = t.Resource_Contact__c;
                provider.Role__c = 'Change Agent';
                provider.TeleMeet__c = t.Id;
                conPar.add(provider);
            }
            //for Navigator
            if (t.Contact__c != null) {
                TeleMeet_Participant__c provider = new TeleMeet_Participant__c();
                provider.Contact__c = t.Contact__c;
                provider.Role__c = 'Contact';
                provider.TeleMeet__c = t.Id;
                conPar.add(provider);
            }


        }
        if (!conPar.isEmpty()) {
            insert conPar;
            sendEmailToNavAgent(Trigger.new, connectNavAgent);
        }
        //for Navigator

    }
    //mel upon schedule 
    public static void sendEmailToNavAgent(List < Telemeet__c > triggerNew, Set<Id> connectNavAgent) {
        List<Contact> conList = [Select Name from Contact Where Id IN: connectNavAgent];
        Map<Id, Contact> conMap = new Map<Id, Contact>();
        
        for(Contact c: conList){
            conMap.put(c.Id, c);
        }

        String TimeZoneSidKey = [SELECT Id, TimeZoneSidKey FROM Organization].TimeZoneSidKey;

        //get timezone from metadata
        Map < String, String > SFTimezone = new Map < String, String > ();
        for (SFTimezone__mdt s: [Select Id, MasterLabel, DeveloperName, Timezone_Name__c From SFTimezone__mdt]) {
            SFTimezone.put(s.MasterLabel, s.Timezone_Name__c);
        }

        Integration_Configuration__mdt ic = [Select Endpoint__c From Integration_Configuration__mdt Where DeveloperName = 'Chime_Integration' LIMIT 1];
        Map < Id, TeleMeet__c > telemeetMap = new Map < Id, TeleMeet__c > ([SELECT Resource_Contact__c, Resource_Contact__r.Name, Meeting_URL2__c, Meeting_URL_for_Email_Notif__c, (SELECT Contact__c, Contact__r.Email, Contact__r.Name, Optional__c, TeleMeet__r.Time_Zone__c FROM TeleMeet_Participants__r WHERE Contact__r.Email != null) FROM TeleMeet__c WHERE Id IN: triggerNew]);
        List < Messaging.SingleEmailMessage > emails = new List < Messaging.SingleEmailMessage > ();
        Id noReplyId = [Select Id From OrgWideEmailAddress Where Address Like '%noreply%'].Id;
        for (TeleMeet__c newTelemeet: triggerNew) {
            if (newTelemeet.Type__c == 'Navigator Connect') {
                TeleMeet__c telemeet = telemeetMap.get(newTelemeet.Id);
                Timezone tz;
                String timeZoneUpdate = '';
                for (TeleMeet_Participant__c participant: telemeet.TeleMeet_Participants__r) {
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    mail.setToAddresses(new List < String > {
                        participant.Contact__r.Email
                    });
                    mail.setSubject(newTelemeet.Subject__c);
                    mail.setHtmlBody(newTelemeet.Description__c);
                    mail.setSaveAsActivity(true);
                    mail.setWhatId(newTelemeet.Id);
                    mail.setTargetObjectId(newTelemeet.Resource_Contact__c);
                    mail.setOrgWideEmailAddressId(noReplyId);

                    DateTime startDT = newTelemeet.Start_Date_Time__c;
                    DateTime endDT = newTelemeet.End_Date_Time__c;


                    system.debug('startDT old>>>' + startDT);
                    system.debug('endDT old>>>' + endDT);


                    String startDTGMT = startDT.format('MM/dd/yyyy/HH/mm/ss', TimeZoneSidKey);
                    String endDTGMT = endDT.format('MM/dd/yyyy/HH/mm/ss', TimeZoneSidKey);

                    system.debug('startDTGMT >>>' + startDTGMT);
                    system.debug('endDTGMT>>>' + endDTGMT);

                    List < String > startDTtz = startDTGMT.split('/');
                    List < String > endDTtz = endDTGMT.split('/');

                    DateTime startDTNew = DateTime.newInstanceGMT(Integer.valueOf(startDTtz[2]), Integer.valueOf(startDTtz[0]), Integer.valueOf(startDTtz[1]), Integer.valueOf(startDTtz[3]),
                        Integer.valueOf(startDTtz[4]), Integer.valueOf(startDTtz[5]));
                    DateTime endDTNew = DateTime.newInstanceGMT(Integer.valueOf(endDTtz[2]), Integer.valueOf(endDTtz[0]), Integer.valueOf(endDTtz[1]), Integer.valueOf(endDTtz[3]),
                        Integer.valueOf(endDTtz[4]), Integer.valueOf(endDTtz[5]));

                    timeZoneUpdate = SFTimezone.get(newTelemeet.Time_Zone__c);
                    tz = Timezone.getTimeZone(timeZoneUpdate);
                    startDT = startDTNew.addHours(-(tz.getOffset(startDTNew) / 3600000));
                    endDT = endDTNew.addHours(-(tz.getOffset(endDTNew) / 3600000));

                    system.debug('startDT>>>' + startDT);
                    system.debug('endDT>>>' + endDT);

                    String startDTConverted = startDT.formatGmt('yyyyMMdd\'T\'HHmmss\'Z\'');
                    String endDTConverted = endDT.formatGmt('yyyyMMdd\'T\'HHmmss\'Z\'');

                    system.debug('startDTConverted>>>' + startDTConverted);
                    system.debug('endDTConverted>>>' + endDTConverted);

                    //Create Meeting Body
                    String meetingInviteBody = '';
                    meetingInviteBody += 'BEGIN:VCALENDAR\n';
                    meetingInviteBody += 'PRODID::-//hacksw/handcal//NONSGML v1.0//EN\n';
                    meetingInviteBody += 'VERSION:2.0\n';
                    meetingInviteBody += 'METHOD:PUBLISH\n';
                    meetingInviteBody += 'X-MS-OLK-FORCEINSPECTOROPEN:TRUE\n';
                    meetingInviteBody += 'BEGIN:VEVENT\n';
                    meetingInviteBody += 'CLASS:PUBLIC\n';
                    meetingInviteBody += 'CREATED:20150126T203709Z\n';
                    meetingInviteBody += 'ORGANIZER;CN=' + (conMap.containsKey(newTelemeet.Resource_Contact__c) ? conMap.get(newTelemeet.Resource_Contact__c).Name : null) + ':MAILTO:changecloud@nationalcoordinationcenter.com\n'; //orgwideMap.get(newSession.Event__r.Email_Sender_Id__c).Address+'\n'; 
                    meetingInviteBody += 'DTEND:' + endDTConverted + '\n';
                    meetingInviteBody += 'DTSTAMP:20150126T203709Z\n';
                    meetingInviteBody += 'DTSTART:' + startDTConverted + '\n';
                    meetingInviteBody += 'LAST-MODIFIED:20150126T203709Z\n';
                    //meetingInviteBody += 'LOCATION:' + ic.Endpoint__c+'?m=' + newTelemeet.Id + '&n=' + (UserInfo.getFirstName()).replace(' ', '%20') + '\n';
                    meetingInviteBody += 'LOCATION:' + telemeet.Meeting_URL_for_Email_Notif__c + '\n';
                    meetingInviteBody += 'PRIORITY:5\n';
                    meetingInviteBody += 'SEQUENCE:0\n';
                    meetingInviteBody += 'SUMMARY:' + newTelemeet.Subject__c + '\n';
                    meetingInviteBody += 'DESCRIPTION:' + newTelemeet.Description__c + '\n';
                    meetingInviteBody += 'LANGUAGE=en-us:Meeting\n';
                    meetingInviteBody += 'TRANSP:OPAQUE\n';
                    meetingInviteBody += 'X-ALT-DESC;FMTTYPE=text/html:<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN"><HTML><HEAD><META NAME="Generator" CONTENT="MS Exchange Server version 08.00.0681.000"><TITLE></TITLE></HEAD><BODY><!-- Converted from text/plain format --></BODY></HTML>\n';
                    meetingInviteBody += 'X-MICROSOFT-CDO-BUSYSTATUS:BUSY\n';
                    meetingInviteBody += 'X-MICROSOFT-CDO-IMPORTANCE:1\n';
                    meetingInviteBody += 'END:VEVENT\n';
                    meetingInviteBody += 'END:VCALENDAR';

                    //Meeting Email Attachment
                    Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();
                    attach.Filename = 'meeting.ics';
                    attach.ContentType = 'text/calendar';
                    attach.Inline = true;
                    attach.Body = Blob.valueOf(meetingInviteBody);

                    //Attach Meeting Attachment
                    mail.setFileAttachments(new Messaging.EmailFileAttachment[] {
                        attach
                    });
                    emails.add(mail);
                }
            }
        }
        Messaging.SendEmailResult[] er = Messaging.sendEmail(emails);

    }
    //mel end

    if (Trigger.isAfter && Trigger.isUpdate) {

        Set < Id > teleMeetids = new Set < Id > ();
        for (TeleMeet__c telemeet: trigger.new) {
            teleMeetids.add(telemeet.Id);
        }
        String TimeZoneSidKey = [SELECT Id, TimeZoneSidKey FROM Organization].TimeZoneSidKey;

        //get timezone from metadata
        Map < String, String > SFTimezone = new Map < String, String > ();
        for (SFTimezone__mdt s: [Select Id, MasterLabel, DeveloperName, Timezone_Name__c From SFTimezone__mdt]) {
            SFTimezone.put(s.MasterLabel, s.Timezone_Name__c);
        }
        Integration_Configuration__mdt ic = [Select Endpoint__c From Integration_Configuration__mdt Where DeveloperName = 'Chime_Integration' LIMIT 1];
        
        Map < Id, TeleMeet__c > telemeetMap = new Map < Id, TeleMeet__c > ([SELECT(SELECT Contact__c, Contact__r.Email, Optional__c, TeleMeet__r.Time_Zone__c FROM TeleMeet_Participants__r WHERE Contact__r.Email != null) FROM TeleMeet__c WHERE Id IN: teleMeetids]);
        List < Messaging.SingleEmailMessage > emails = new List < Messaging.SingleEmailMessage > ();
        for (TeleMeet__c newTelemeet: trigger.new) {
            TeleMeet__c oldTeleMeet = trigger.oldMap.get(newTelemeet.Id);

            if (newTelemeet.Status__c == 'Scheduled' && newTelemeet.Status__c != oldTeleMeet.Status__c) {
                TeleMeet__c telemeet = telemeetMap.get(newTelemeet.Id);
                Timezone tz;
                String timeZoneUpdate = '';
                for (TeleMeet_Participant__c participant: telemeet.TeleMeet_Participants__r) {
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    mail.setToAddresses(new List < String > {
                        participant.Contact__r.Email
                    });
                    mail.setSubject(newTelemeet.Subject__c);
                    mail.setHtmlBody(newTelemeet.Description__c);
                    mail.setSaveAsActivity(true);
                    mail.setWhatId(newTelemeet.Id);
                    mail.setTargetObjectId(participant.Contact__c);

                    DateTime startDT = newTelemeet.Start_Date_Time__c;
                    DateTime endDT = newTelemeet.End_Date_Time__c;


                    system.debug('startDT old>>>' + startDT);
                    system.debug('endDT old>>>' + endDT);


                    String startDTGMT = startDT.format('MM/dd/yyyy/HH/mm/ss', TimeZoneSidKey);
                    String endDTGMT = endDT.format('MM/dd/yyyy/HH/mm/ss', TimeZoneSidKey);

                    system.debug('startDTGMT >>>' + startDTGMT);
                    system.debug('endDTGMT>>>' + endDTGMT);

                    List < String > startDTtz = startDTGMT.split('/');
                    List < String > endDTtz = endDTGMT.split('/');

                    DateTime startDTNew = DateTime.newInstanceGMT(Integer.valueOf(startDTtz[2]), Integer.valueOf(startDTtz[0]), Integer.valueOf(startDTtz[1]), Integer.valueOf(startDTtz[3]),
                        Integer.valueOf(startDTtz[4]), Integer.valueOf(startDTtz[5]));
                    DateTime endDTNew = DateTime.newInstanceGMT(Integer.valueOf(endDTtz[2]), Integer.valueOf(endDTtz[0]), Integer.valueOf(endDTtz[1]), Integer.valueOf(endDTtz[3]),
                        Integer.valueOf(endDTtz[4]), Integer.valueOf(endDTtz[5]));

                    timeZoneUpdate = SFTimezone.get(newTelemeet.Time_Zone__c);
                    tz = Timezone.getTimeZone(timeZoneUpdate);
                    startDT = startDTNew.addHours(-(tz.getOffset(startDTNew) / 3600000));
                    endDT = endDTNew.addHours(-(tz.getOffset(endDTNew) / 3600000));

                    system.debug('startDT>>>' + startDT);
                    system.debug('endDT>>>' + endDT);

                    String startDTConverted = startDT.formatGmt('yyyyMMdd\'T\'HHmmss\'Z\'');
                    String endDTConverted = endDT.formatGmt('yyyyMMdd\'T\'HHmmss\'Z\'');

                    system.debug('startDTConverted>>>' + startDTConverted);
                    system.debug('endDTConverted>>>' + endDTConverted);

                    //Create Meeting Body
                    String meetingInviteBody = '';
                    meetingInviteBody += 'BEGIN:VCALENDAR\n';
                    meetingInviteBody += 'PRODID::-//hacksw/handcal//NONSGML v1.0//EN\n';
                    meetingInviteBody += 'VERSION:2.0\n';
                    meetingInviteBody += 'METHOD:PUBLISH\n';
                    meetingInviteBody += 'X-MS-OLK-FORCEINSPECTOROPEN:TRUE\n';
                    meetingInviteBody += 'BEGIN:VEVENT\n';
                    meetingInviteBody += 'CLASS:PUBLIC\n';
                    meetingInviteBody += 'CREATED:20150126T203709Z\n';
                    meetingInviteBody += 'DTEND:' + endDTConverted + '\n';
                    meetingInviteBody += 'DTSTAMP:20150126T203709Z\n';
                    meetingInviteBody += 'DTSTART:' + startDTConverted + '\n';
                    meetingInviteBody += 'LAST-MODIFIED:20150126T203709Z\n';
                    meetingInviteBody += 'LOCATION:' + ic.Endpoint__c+'?m=' + newTelemeet.Id + '&n=' + (UserInfo.getFirstName()).replace(' ', '%20') + '\n';
                    meetingInviteBody += 'PRIORITY:5\n';
                    meetingInviteBody += 'SEQUENCE:0\n';
                    meetingInviteBody += 'SUMMARY:' + newTelemeet.Subject__c + '\n';
                    meetingInviteBody += 'DESCRIPTION:' + newTelemeet.Description__c + '\n';
                    meetingInviteBody += 'LANGUAGE=en-us:Meeting\n';
                    meetingInviteBody += 'TRANSP:OPAQUE\n';
                    meetingInviteBody += 'X-ALT-DESC;FMTTYPE=text/html:<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN"><HTML><HEAD><META NAME="Generator" CONTENT="MS Exchange Server version 08.00.0681.000"><TITLE></TITLE></HEAD><BODY><!-- Converted from text/plain format --></BODY></HTML>\n';
                    meetingInviteBody += 'X-MICROSOFT-CDO-BUSYSTATUS:BUSY\n';
                    meetingInviteBody += 'X-MICROSOFT-CDO-IMPORTANCE:1\n';
                    meetingInviteBody += 'END:VEVENT\n';
                    meetingInviteBody += 'END:VCALENDAR';

                    //Meeting Email Attachment
                    Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();
                    attach.Filename = 'meeting.ics';
                    attach.ContentType = 'text/calendar';
                    attach.Inline = true;
                    attach.Body = Blob.valueOf(meetingInviteBody);

                    //Attach Meeting Attachment
                    mail.setFileAttachments(new Messaging.EmailFileAttachment[] {
                        attach
                    });
                    emails.add(mail);
                }
            }
        }
        Messaging.SendEmailResult[] er = Messaging.sendEmail(emails);
    }

}