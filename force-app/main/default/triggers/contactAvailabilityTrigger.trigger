trigger contactAvailabilityTrigger on Contact_Availability__c (before insert, before update) {
    
    if (Trigger.isBefore) {
        if(Trigger.isInsert) {
            Set<String> conAvaChk = new Set<String>();
            for(Contact_Availability__c c: trigger.new){
                conAvaChk.add((String.valueOf(c.Contact__c)).substring(0, 15)+''+c.Start_Date_Time__c+'Z');
            }
            List<Contact_Availability__c> conAvaList = [SELECT Id, ContactStartTime__c FROM Contact_Availability__c WHERE ContactStartTime__c IN: conAvaChk];
            Map<String, Contact_Availability__c> conAvaMap = new Map<String, Contact_Availability__c>();
            
            for(Contact_Availability__c c: conAvaList){
                conAvaMap.put(c.ContactStartTime__c, c);
            }
            for(Contact_Availability__c c: trigger.new){
                if(conAvaMap.containsKey((String.valueOf(c.Contact__c)).substring(0, 15)+''+c.Start_Date_Time__c+'Z')){
                    c.addError('Contact Availability already have a schedule.');
                }
            }
        }
    }
}