trigger EventTrigger on Event__c (before insert,before update) {

    Apex_Trigger_Switch__c switchh = Apex_Trigger_Switch__c.getInstance('EventTrigger');
    if(switchh != null && !switchh.Active__c){
        return;
    }
    
    CCEventTriggerHandler handler = new CCEventTriggerHandler();

	if(Trigger.isInsert  && Trigger.isBefore){
		handler.OnBeforeInsert(Trigger.new);
	}
	if(Trigger.isUpdate  && Trigger.isBefore){
		handler.OnBeforeUpdate(Trigger.newMap, Trigger.oldMap);
	}
    
}