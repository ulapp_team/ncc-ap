@istest
public class CampaignTriggerTest {
    @isTest
    public static void testCampaignTrigger() {
        
        List<campaign__c> lstC;
        
        // insert campaign
        campaign__c c = new campaign__c(name = 'test campaign');
        insert c;
        lstC =[select id from campaign__c where id=: c.id];
        system.assertEquals(1,lstC.size() );
        
        // update campaign
        c.Name = 'test campaign Updated';
        update c;
        lstC =[select id, name from campaign__c where id=: c.id];
        system.assertEquals('test campaign Updated',lstC[0].Name );
        
        // delete campaign
        delete c;
        lstC =[select id from campaign__c where id=: c.id];
        system.assertEquals(0,lstC.size() );
        
        // undelete campaign
        undelete c;
        lstC =[select id from campaign__c where id=: c.id];
        system.assertEquals(1,lstC.size() );
        
        
    }
    
    @isTest
    public static void testgetCommunityURL() {
        string commURL = CampaignTriggerUtility.getCommunityURL('xxx');
        System.assert(commURL.contains('TRY-CATCH-Exception'),commURL );
    }
}