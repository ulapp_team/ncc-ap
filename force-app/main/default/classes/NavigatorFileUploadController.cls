/*******************************************************************************************
* @name: NavigatorFileUploadController
* @author: JaysonLabnao
* @created: 26-08-2021
* @description: Controller Class for Navigator File Upload LWC.
*
* Changes (version)
* -------------------------------------------------------------------------------------------
*            No.   Date(dd-mm-yyy)  Author                Description
*            ----  ---------        --------------------  -----------------------------
* @version   1.0   26-08-2021       Jayson Labnao         Initial version.
*********************************************************************************************/
public with sharing class NavigatorFileUploadController {

    /*******************************************************************************************
    * @name: OptionWrapper
    * @author: JaysonLabnao
    * @created: 26-08-2021
    * @description: Wrapper class to return options for picklist/combobox element.
    *
    * Changes (version)
    * -------------------------------------------------------------------------------------------
    *            No.   Date(dd-mm-yyy)  Author                Description
    *            ----  ---------        --------------------  -----------------------------
    * @version   1.0   26-08-2021       Jayson Labnao         Initial version.
    *********************************************************************************************/
    public class OptionWrapper{
        @AuraEnabled public String label;
        @AuraEnabled public String value;
    }

    /*******************************************************************************************
    * @name: updateFieldUrl
    * @author: JaysonLabnao
    * @created: 26-08-2021
    * @description: Replaces the url value of a URL field with the external user shareable link.
    *
    * Changes (version)
    * -------------------------------------------------------------------------------------------
    *            No.   Date(dd-mm-yyy)  Author                Description
    *            ----  ---------        --------------------  -----------------------------
    * @version   1.0   26-08-2021       Jayson Labnao         Initial version.
    *********************************************************************************************/
    @AuraEnabled
    public static String updateFieldUrl(String documentId, String navigatorId, String fieldApiName){
        String response = 'Success';
        try{
            ContentVersion contentVer = [ SELECT Id, ContentDocument.Title FROM ContentVersion WHERE ContentDocumentId = :documentId];
    
            ContentDistribution contentDist = new ContentDistribution();
            contentDist.Name = contentVer.ContentDocument.Title;
            contentDist.ContentVersionId = contentVer.Id;
            contentDist.PreferencesNotifyOnVisit = false;
            contentDist.PreferencesLinkLatestVersion = true;
            Insert contentDist;
    
            contentDist = [SELECT Id, ContentDownloadUrl FROM ContentDistribution WHERE Id = :contentDist.Id ];
    
            Navigator__c nav = new Navigator__c();
            nav.Id = navigatorId;
            nav.put(fieldApiName, contentDist.ContentDownloadUrl);
            Update nav;
        }
        catch(Exception e){
            response = 'Error: ' + e.getMessage() + ' Stack Trace: ' + e.getStackTraceString();
        }

        return response;
    }

    /*******************************************************************************************
    * @name: updateObjectFieldUrl
    * @author: JaysonLabnao
    * @created: 26-08-2021
    * @description: Replaces the url value of a URL field with the external user shareable link.
    *               Can be use with any objects that has URL Field/s.
    *
    * Changes (version)
    * -------------------------------------------------------------------------------------------
    *            No.   Date(dd-mm-yyy)  Author                Description
    *            ----  ---------        --------------------  -----------------------------
    * @version   1.0   26-08-2021       Jayson Labnao         Initial version.
    *********************************************************************************************/
    @AuraEnabled
    public static String updateObjectFieldUrl(String documentId, String recordId, String objectApiName, String fieldApiName){
        String response = 'Success';
        try{
            ContentVersion contentVer = [ SELECT Id, ContentDocument.Title FROM ContentVersion WHERE ContentDocumentId =: documentId];
    
            ContentDistribution contentDist = new ContentDistribution();
            contentDist.Name = contentVer.ContentDocument.Title;
            contentDist.ContentVersionId = contentVer.Id;
            contentDist.PreferencesNotifyOnVisit = false;
            contentDist.PreferencesLinkLatestVersion = true;
            Insert contentDist;
            contentDist = [SELECT Id, ContentDownloadUrl FROM ContentDistribution WHERE Id = :contentDist.Id ];
            
            sObject obj = Schema.getGlobalDescribe().get(objectApiName).newSObject();
            obj.Id = recordId;
            obj.put(fieldApiName, contentDist.ContentDownloadUrl);
            Update obj;
        }
        catch(Exception e){
            response = 'Error: ' + e.getMessage() + ' Stack Trace: ' + e.getStackTraceString();
        }

        return response;
    }

    /*******************************************************************************************
    * @name: updateObjectFieldUrl
    * @author: JaysonLabnao
    * @created: 26-08-2021
    * @description: Retrieves List of Option Wrapper class. Used for populating options for
    *               combobox/picklist elements.
    *
    * Changes (version)
    * -------------------------------------------------------------------------------------------
    *            No.   Date(dd-mm-yyy)  Author                Description
    *            ----  ---------        --------------------  -----------------------------
    * @version   1.0   26-08-2021       Jayson Labnao         Initial version.
    *********************************************************************************************/
    @AuraEnabled
    public static List<OptionWrapper> getOptions(String recordId){
        final Id objectId = recordId; 
        
        Schema.SObjectType token = objectId.getSObjectType();
        Schema.DescribeSObjectResult dr = token.getDescribe();
        Map<String, Schema.SObjectField> fieldsMap = dr.fields.getMap();

        List<OptionWrapper> newOptions = new List<OptionWrapper>();
        for(String field : fieldsMap.keySet()){
            OptionWrapper option = new OptionWrapper();
            Schema.DisplayType type = fieldsMap.get(field).getDescribe().getType();
            String label = fieldsMap.get(field).getDescribe().getLabel();
            if(type == Schema.DisplayType.URL){
                option.label = label;
                option.value = field;
                newOptions.add(option);
            }
        }
        return newOptions;
    }
}