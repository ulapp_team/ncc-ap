public class countdowntimercontroller { 
    @AuraEnabled
    public static TeleMeet__c fetchTelemeetStartDate(String recId){
        TeleMeet__c tel = [Select Meeting_URL2__c, Start_Date_Time__c, End_Date_Time__c, Time_Zone__c, 
                           Encounter__c, Encounter__r.Reason__c, Encounter__r.Service__r.Name,
                           Contact__c, Contact__r.Name, Contact__r.FirstName, Contact__r.Profile_Picture_URL__c, 
                           Resource_Contact__c, Resource_Contact__r.Name, Resource_Contact__r.Profile_Picture_URL__c, 
                           Show_in_Lobby__c, Health_Summary__c, My_Medications__c, Patient_Problems__c, HPI_Notes__c,
                           Practice_Management__c, Practice_Management__r.Name from TeleMeet__c where id =: recId];
        
        return tel;
    }
    
    @AuraEnabled
    public static List<Connect_Agenda__c> fetchConnectAgenda(String recId){
        Telemeet__c connect = [Select Id, Practice_Management__c From Telemeet__c Where Id =: recId Limit 1];
        List<Connect_Agenda__c> conAge = [select Completed__c, Connect__c, Contact__c, Contact__r.Name, Description__c, End_Time__c, Start_Time__c from Connect_Agenda__c where Practice_Management__c =: connect.Practice_Management__c AND Start_Time__c != null AND End_Time__c != null order by Start_Time__c];
        return conAge;
    }
    
    @AuraEnabled
    public static List<TeleMeet_Participant__c> fetchConnectParticipant(String recId){
        List<TeleMeet_Participant__c> conPart = [select Contact__c, Contact__r.Name, Contact__r.Profile_Picture_URL__c, Optional__c, Role__c from TeleMeet_Participant__c  where TeleMeet__c =: recId order by CreatedDate ASC];
        return conPart;
    }
    
    @AuraEnabled
    public static List<Telehealth_Consultation_Group__c> fetchTelehealthConsultationGroup(String recId){
        List<Telehealth_Consultation_Group__c> conPart = [select Contact__c, Contact__r.Name, Contact__r.Profile_Picture_URL__c, Role__c from Telehealth_Consultation_Group__c  where Practice_Management__c =: recId order by CreatedDate ASC];
        return conPart;
    }
    
    @AuraEnabled
    public static void createConnectNotes(String connectId, String myMedications){
        Note addedntes = new Note();
        addedntes.ParentId = connectId;
        addedntes.Body = myMedications;
        addedntes.Title = 'My Medications';
        
        
        insert addedntes;
    }
    
    @AuraEnabled
    public static List<ContentDocumentLink> fetchConnectFiles(String connectId){
    	List<ContentDocumentLink> cdl = [SELECT Id, ContentDocument.Title, ContentDocument.FileType, ContentDocumentId, LinkedEntityId  FROM ContentDocumentLink where LinkedEntityId in ( SELECT Id FROM Telemeet__c WHERE Id =: connectId) and LinkedEntity.Type='Telemeet__c'];
    	//List<ContentDocument> cd = [Select Id, Title, FileExtension, CreatedDate From ContentDocument];
    	
        return cdl;    
    }
    
    
    @AuraEnabled
    public static Connect_Agenda__c searchForConAgeRecord(String recordId){
    	Connect_Agenda__c conAge = [SELECT Completed__c, Contact__c, Description__c, End_Time__c, Start_Time__c FROM Connect_Agenda__c WHERE Id =: recordId];
        
        return conAge;
    }
    
    @AuraEnabled
    public static String completeConnectAgenda(String recordId){
    	Connect_Agenda__c conAge = [SELECT Completed__c, Contact__c, Description__c, End_Time__c, Start_Time__c FROM Connect_Agenda__c WHERE Id =: recordId];
        conAge.Completed__c = true;
        update conAge;
        return 'success';
    }
    
    //Get SFTimezone via Metadata
    @AuraEnabled        
    public static Map<String, String> getSFTimezone(){
        Map<String, String> SFTimezoneMap = new Map<String, String>();
        List<SFTimezone__mdt> sft = [SELECT DeveloperName, Timezone_Name__c FROM SFTimezone__mdt ORDER BY DeveloperName]; 
        for(SFTimezone__mdt s: sft){
            SFTimezoneMap.put(s.DeveloperName, s.Timezone_Name__c);
        }
        
        return SFTimezoneMap;
    }
    
    
    @AuraEnabled
    public static String getTimeZoneSidKey(){
        
    	String TimeZoneSidKey =  [SELECT Id, TimeZoneSidKey FROM Organization].TimeZoneSidKey;
        
    	return TimeZoneSidKey;
    }
    
    
    @AuraEnabled
    public static Map<String, String> getConvertedDates(DateTime StartDate, String TelTimeZone){
        Map<String, String> dates = new Map<String, String>();
        
    	String TimeZoneSidKey =  [SELECT Id, TimeZoneSidKey FROM Organization].TimeZoneSidKey;
        
        dates.put('TimeZoneSidKey', TimeZoneSidKey);
        
        
		DateTime startDT;
        Timezone tz;
        String startDTGMT = StartDate.format('MM/dd/yyyy/HH/mm/ss', TimeZoneSidKey);
        
        system.debug('Date Time Now >>>'+system.now() );
        system.debug('startDTGMT >>>'+startDTGMT );
        
        List<String> startDTtz = startDTGMT.split('/');
        
        DateTime startDTNew = DateTime.newInstanceGMT(Integer.valueOf(startDTtz[2]), Integer.valueOf(startDTtz[0]), Integer.valueOf(startDTtz[1]),  Integer.valueOf(startDTtz[3]), 
                                                      Integer.valueOf(startDTtz[4]), Integer.valueOf(startDTtz[5]));
        
        /*tz = Timezone.getTimeZone(TelTimeZone);
        startDT = startDTNew.addHours(-(tz.getOffset(startDTNew)/3600000));*/
        
        
        system.debug('startDT Converted >>>'+startDTNew );
        
		dates.put('StartDate', String.valueOf(startDTNew));        
        
        
    	return dates;
    }
}