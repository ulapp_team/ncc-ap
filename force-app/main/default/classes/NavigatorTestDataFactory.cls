/*******************************************************************************************
* @name: NavigatorTestDataFactory
* @author: JaysonLabnao
* @created: 14-02-2021
* @description: Test Data Factory Class for Navigator Features
*
* Changes (version)
* -------------------------------------------------------------------------------------------
*            No.   Date(dd-mm-yyy)  Author                Description
*            ----  --------------   --------------------  -----------------------------
* @version   1.0   14-02-2021       Jayson Labnao         [01055] Initial version.
*********************************************************************************************/
@isTest
public with sharing class NavigatorTestDataFactory {

    /*******************************************************************************************
    * @name: createContact
    * @author: JaysonLabnao
    * @created: 14-02-2021
    * @description: Creates test contact record for Navigator Test Classes.
    *
    * Changes (version)
    * -------------------------------------------------------------------------------------------
    *            No.   Date(dd-mm-yyy)  Author                Description
    *            ----  --------------   --------------------  -----------------------------
    * @version   1.0   14-02-2021       Jayson Labnao         [01055] Initial version.
    *********************************************************************************************/
    public static Contact createContact(String lastName, String email, String contactId){
        Contact testContact = new Contact();
        testContact.LastName = lastName;
        testContact.Email = email;
        testContact.Contact_Id__c = contactId;
        return testContact;
    }

    /*******************************************************************************************
    * @name: createMultipleContact
    * @author: JaysonLabnao
    * @created: 14-02-2021
    * @description: Creates multiple test contact records for Navigator Test Classes.
    *
    * Changes (version)
    * -------------------------------------------------------------------------------------------
    *            No.   Date(dd-mm-yyy)  Author                Description
    *            ----  --------------   --------------------  -----------------------------
    * @version   1.0   14-02-2021       Jayson Labnao         [01055] Initial version.
    *********************************************************************************************/
    public static List<Contact> createMultipleContact(String lastName, Integer numOfContacts){
        List<Contact> newContacts = new List<Contact>();
        for(Integer i=0; i<numOfContacts; i++){
            newContacts.add(createContact(lastName+i, lastName+i+'@test.com', null));
        }
        return newContacts;
    }

    /*******************************************************************************************
    * @name: createCampaign
    * @author: JaysonLabnao
    * @created: 14-02-2021
    * @description: Creates test campaign record for Navigator Test Classes.
    *
    * Changes (version)
    * -------------------------------------------------------------------------------------------
    *            No.   Date(dd-mm-yyy)  Author                Description
    *            ----  --------------   --------------------  -----------------------------
    * @version   1.0   14-02-2021       Jayson Labnao         [01055] Initial version.
    *********************************************************************************************/
    public static Campaign__c createCampaign(String name){
        Campaign__c testCampaign = new Campaign__c();
        testCampaign.Name = name;
        return testCampaign;
    }

    /*******************************************************************************************
    * @name: createJourney
    * @author: JaysonLabnao
    * @created: 14-02-2021
    * @description: Creates test Journey record for Navigator Test Classes.
    *
    * Changes (version)
    * -------------------------------------------------------------------------------------------
    *            No.   Date(dd-mm-yyy)  Author                Description
    *            ----  --------------   --------------------  -----------------------------
    * @version   1.0   14-02-2021       Jayson Labnao         [01055] Initial version.
    *********************************************************************************************/
    public static Journey__c createJourney(String name, String campaignId, String status){
        Journey__c testJourney = new Journey__c();
        testJourney.Name = name;
        testJourney.Campaign__c = campaignId;
        testJourney.Status__c = status;
        return testJourney;
    }

    /*******************************************************************************************
    * @name: createJourneyParticipant
    * @author: JaysonLabnao
    * @created: 14-02-2021
    * @description: Creates test Journey Participant record for Navigator Test Classes.
    *
    * Changes (version)
    * -------------------------------------------------------------------------------------------
    *            No.   Date(dd-mm-yyy)  Author                Description
    *            ----  --------------   --------------------  -----------------------------
    * @version   1.0   14-02-2021       Jayson Labnao         [01055] Initial version.
    *********************************************************************************************/
    public static Journey_Participant__c createJourneyParticipant(String journeyId, String contactId){
        Journey_Participant__c testParticipant = new Journey_Participant__c();
        testParticipant.Journey__c = journeyId;
        testParticipant.Contact__c = contactId;
        return testParticipant;
    }

    /*******************************************************************************************
    * @name: createEvent
    * @author: JaysonLabnao
    * @created: 14-02-2021
    * @description: Creates test Event record for Navigator Test Classes.
    *
    * Changes (version)
    * -------------------------------------------------------------------------------------------
    *            No.   Date(dd-mm-yyy)  Author                Description
    *            ----  --------------   --------------------  -----------------------------
    * @version   1.0   14-02-2021       Jayson Labnao         [01055] Initial version.
    *********************************************************************************************/
    public static Event__c createEvent(String name, String campaignId, String eventId, Date startDate){
        Event__c evt = new Event__c();
        evt.Name = name;
        evt.Campaign__c = campaignId;
        evt.Event_Id__c = eventId;
        evt.IsActive__c = true;
        evt.Start_Date_Time__c = startDate;
        evt.End_Date_Time__c = startDate.addDays(1);
        return evt;
    }

    /*******************************************************************************************
    * @name: createEventParticipant
    * @author: JaysonLabnao
    * @created: 14-02-2021
    * @description: Creates test Event Participant record for Navigator Test Classes.
    *
    * Changes (version)
    * -------------------------------------------------------------------------------------------
    *            No.   Date(dd-mm-yyy)  Author                Description
    *            ----  --------------   --------------------  -----------------------------
    * @version   1.0   14-02-2021       Jayson Labnao         [01055] Initial version.
    *********************************************************************************************/
    public static Participant__c createEventParticipant(String eventId, String contactId, String status){
        Participant__c testEventParticipant = new Participant__c();
        testEventParticipant.Event__c = eventId;
        testEventParticipant.Member_Contact__c = contactId;
        testEventParticipant.Status__c = status;
        return testEventParticipant;
    }

    /*******************************************************************************************
    * @name: createSession
    * @author: JaysonLabnao
    * @created: 14-02-2021
    * @description: Creates test Session record for Navigator Test Classes.
    *
    * Changes (version)
    * -------------------------------------------------------------------------------------------
    *            No.   Date(dd-mm-yyy)  Author                Description
    *            ----  --------------   --------------------  -----------------------------
    * @version   1.0   14-02-2021       Jayson Labnao         [01055] Initial version.
    *********************************************************************************************/
    public static Session__c createSession(String name, String eventId, DateTime startDateTime){
        Session__c session = new Session__c();
        session.Name = name;
        session.Event__c = eventId;
        session.IsActive__c = true;
        // Month/Day/Year;
        session.Start_Date_Time__c = startDateTime;
        session.End_Date_Time__c = startDateTime.addHours(1);
        return session;
    }

    /*******************************************************************************************
    * @name: createSessionParticipant
    * @author: JaysonLabnao
    * @created: 14-02-2021
    * @description: Creates test Session Participant record for Navigator Test Classes.
    *
    * Changes (version)
    * -------------------------------------------------------------------------------------------
    *            No.   Date(dd-mm-yyy)  Author                Description
    *            ----  --------------   --------------------  -----------------------------
    * @version   1.0   14-02-2021       Jayson Labnao         [01055] Initial version.
    *********************************************************************************************/
    public static Session_Participant__c createSessionParticipant(String sessionId, String eventParticipantId, String status){
        Session_Participant__c sParticipant = new Session_Participant__c();
        sParticipant.Session__c = sessionId;
        sParticipant.Participant__c = eventParticipantId;
        sParticipant.Status__c = status;
        return sParticipant;
    }

    /*******************************************************************************************
    * @name: createMilestone
    * @author: JaysonLabnao
    * @created: 14-02-2021
    * @description: Creates test Milestone record for Navigator Test Classes.
    *
    * Changes (version)
    * -------------------------------------------------------------------------------------------
    *            No.   Date(dd-mm-yyy)  Author                Description
    *            ----  --------------   --------------------  -----------------------------
    * @version   1.0   14-02-2021       Jayson Labnao         [01055] Initial version.
    *********************************************************************************************/
    public static Milestone__c createMilestone(String name, String journeyId, String type, String relatedRecordId){
        Milestone__c mStone = new Milestone__c();
        mStone.Name = name;
        mStone.Journey__c = journeyId;
        mStone.Type__c = type;
        mStone.Related_RecordId__c = relatedRecordId;
        return mStone;
    }

    /*******************************************************************************************
    * @name: createParticipantMilestone
    * @author: JaysonLabnao
    * @created: 14-02-2021
    * @description: Creates test Participant Milestone record for Navigator Test Classes.
    *
    * Changes (version)
    * -------------------------------------------------------------------------------------------
    *            No.   Date(dd-mm-yyy)  Author                Description
    *            ----  --------------   --------------------  -----------------------------
    * @version   1.0   14-02-2021       Jayson Labnao         [01055] Initial version.
    *********************************************************************************************/
    public static Participant_Milestone__c createParticipantMilestone(String contactId, String participantId, String type){
        Participant_Milestone__c pm = new Participant_Milestone__c();
        pm.Contact__c = contactId;
        pm.Journey__c = participantId;
        pm.Type__c = type;
        return pm;
    }

    /*******************************************************************************************
    * @name: createNavigator
    * @author: JaysonLabnao
    * @created: 14-02-2021
    * @description: Creates test Navigator record for Navigator Test Classes.
    *
    * Changes (version)
    * -------------------------------------------------------------------------------------------
    *            No.   Date(dd-mm-yyy)  Author                Description
    *            ----  --------------   --------------------  -----------------------------
    * @version   1.0   14-02-2021       Jayson Labnao         [01055] Initial version.
    *********************************************************************************************/
    public static Navigator__c createNavigator(String name, String campaignId){
        Navigator__c nav = new Navigator__c();
        nav.Name = name;
        nav.Campaign__c = campaignId;
        return nav;
    }

    /*******************************************************************************************
    * @name: createNavigatorItem
    * @author: JaysonLabnao
    * @created: 23-02-2021
    * @description: Creates test Navigator Items/Tabs record for Navigator Test Classes.
    *
    * Changes (version)
    * -------------------------------------------------------------------------------------------
    *            No.   Date(dd-mm-yyy)  Author                Description
    *            ----  --------------   --------------------  -----------------------------
    * @version   1.0   23-02-2021       Jayson Labnao         Initial version.
    *********************************************************************************************/
    public static Navigator_Item__c createNavigatorItem(String name, String tabLabel, String navigatorId, Integer sortOrder){
        Navigator_Item__c navTab = new Navigator_Item__c();
        navTab.Name = name;
        navTab.Label__c = tabLabel;
        navTab.Navigator__c = navigatorId;
        navTab.Site_Page_Name__c = name;
        navTab.Sort_Order__c = sortOrder;
        navTab.Page_URL__c = 'https://TestURL.com';
        return navTab;
    }

    /*******************************************************************************************
    * @name: createNavigatorSection
    * @author: JaysonLabnao
    * @created: 24-02-2021
    * @description: Creates test Navigator Section for Navigator Test Classes.
    *
    * Changes (version)
    * -------------------------------------------------------------------------------------------
    *            No.   Date(dd-mm-yyy)  Author                Description
    *            ----  --------------   --------------------  -----------------------------
    * @version   1.0   24-02-2021       Jayson Labnao         Initial version.
    *********************************************************************************************/
    public static Navigator_Section__c createNavigatorSection(String name, String navigatorItemId, Integer sortOrder){
        Navigator_Section__c navSection = new Navigator_Section__c();
        navSection.Name = name;
        navSection.Sort_Order__c = sortOrder;
        navSection.Navigator_Item__c = navigatorItemId;
        return navSection;
    }

    /*******************************************************************************************
    * @name: createNavigatorSectionAttribute
    * @author: JaysonLabnao
    * @created: 24-02-2021
    * @description: Creates test Navigator Section Attribute for Navigator Test Classes.
    *
    * Changes (version)
    * -------------------------------------------------------------------------------------------
    *            No.   Date(dd-mm-yyy)  Author                Description
    *            ----  --------------   --------------------  -----------------------------
    * @version   1.0   24-02-2021       Jayson Labnao         Initial version.
    *********************************************************************************************/
    public static Navigator_Section_Attribute__c createNavigatorSectionAttribute(String name, String navigatorSectionId, String type, Integer sortOrder, String value){
        Navigator_Section_Attribute__c navSectionAttr = new Navigator_Section_Attribute__c();
        navSectionAttr.Navigator_Section__c = navigatorSectionId;
        navSectionAttr.Name = name;
        navSectionAttr.Type__c = type;
        navSectionAttr.Sort_Order__c = sortOrder;
        navSectionAttr.Value__c = value;
        return navSectionAttr;
    }
}