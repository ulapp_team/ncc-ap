@isTest
public class TaskTriggerHandlerTest {
    
    @IsTest
    static void testSendEmailAfterContactUsTask(){
        
        EmailTemplate e = new EmailTemplate (developerName = 'test', FolderId = UserInfo.getUserId(), TemplateType= 'Text', Name = 'test', IsActive = true);
        insert e;
        
        EmailTemplate e2 = new EmailTemplate (developerName = 'ContactUsTaskEmailTemplate', FolderId = UserInfo.getUserId(), TemplateType= 'Text', Name = 'Contact Us Task Email Template', IsActive = true);
        insert e2;
        
        OrgWideEmailAddress owa = [SELECT Id, Address, DisplayName FROM OrgWideEmailAddress LIMIT 1];

        Compass_Setting__c compSetting = new Compass_Setting__c();
        compSetting.Name = 'Default Settings';
        compSetting.Email_Sender_Id__c = owa.Id;
        compSetting.Email_Sender_Name__c = 'NAME';
        compSetting.Email_Template_Invitation_Id__c = e.Id;
        compSetting.Email_Template_Registration_Id__c = e.Id;
        insert compSetting;
        
        UserRole r = new UserRole(DeveloperName = 'TestCEO', Name = 'Test CEO');
        insert r;

        User ceoUser = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
            LastName = 'last',
            Email = 'testuser@cerner.com',
            Username = 'testuser@cerner.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = r.Id
        );
        
        User contactUsUser = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
            LastName = 'last',
            Email = 'testcontactUs@cerner.com',
            Username = 'testcontactUs@cerner.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US'
        );
        insert contactUsUser;
                
        System.runAs ( ceoUser ) {            
            Account testAccount = new Account(name ='Ulapp');
            insert testAccount; 
            
            Contact testContact = new Contact(LastName ='testCon',AccountId = testAccount.Id, Email='test1@com.com');
            insert testContact;  
            
            Campaign__c testCampaign = new Campaign__c();
            testCampaign.Name = 'Test Campaign Name';
            insert testCampaign;
            
            Journey__c testJourney = new Journey__c();
            testJourney.Name = 'Test Journey';
            testJourney.Campaign__c = testCampaign.Id;
            testJourney.Status__c = 'For Review';
            testJourney.End_Date__c = System.today() + 3;
            testJourney.Contact_Us_User__c = contactUsUser.Id;
            insert testJourney;
        }

        Id contactUsTaskRecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Community Journey - Contact Us').getRecordTypeId();
        
        Test.startTest();
            Contact testContact = [SELECT Id, Name, Email, Phone FROM Contact Where LastName ='testCon' LIMIT 1];
            Journey__c testJournery = [SELECT Id, Name, Contact_Us_User__c, Contact_Us_User__r.Name, Contact_Us_User__r.FirstName, Contact_Us_User__r.LastName FROM Journey__c Where Contact_Us_User__c != null limit 1];
            Task testTaskData = new Task();
            testTaskData.RecordTypeId = contactUsTaskRecordTypeId;
            testTaskData.OwnerId = UserInfo.getUserId();
            testTaskData.WhoId = testContact.Id;
            testTaskData.WhatId = testJournery.Id;

        Test.stopTest();
    }
    
}