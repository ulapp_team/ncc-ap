@IsTest(IsParallel=true)
private with sharing class FlsUtilsTest {
  @TestSetup
  static void setupData() {
    Campaign__c campaignRec = new Campaign__c();
    insert campaignRec;

    Event__c evt = new Event__c(
      Name = 'Test Event',
      Campaign__c = campaignRec.Id,
      Event_Id__c = 'Test'
    );
    insert evt;
  }

  @IsTest
  static void testAccess() {
    Test.startTest();
    Event__c evtRec = [
      SELECT Id, CreatedDate, Name
      FROM Event__c
      WHERE Name = 'Test Event'
    ];
    String str = FlsUtils.prefix;
    Test.stopTest();

    System.assertEquals(
      true,
      FlsUtils.isAccessible('Event__c', new List<String>{ 'Name' })
    );
    System.assertEquals(
      true,
      FlsUtils.isUpdateable(evtRec, new List<String>{ 'Name' })
    );
    System.assertEquals(
      true,
      FlsUtils.isCreateable(evtRec, new List<String>{ 'Name' })
    );
    System.assertEquals(true, FlsUtils.isDeletable(evtRec));
  }
}