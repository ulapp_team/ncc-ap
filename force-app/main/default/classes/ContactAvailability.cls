public class ContactAvailability {
    @AuraEnabled
    public static List<Contact_Availability__c> getContactAvailability(String conId){
        List<Contact_Availability__c> conAva = [Select Id, Contact__c, Time_Zone__c, Start_Date_Time__c, End_Date_Time__c, Status__c From Contact_Availability__c Where Contact__c =: conId Order By Start_Date_Time__c];
        
        return conAva;
    }
    
    @AuraEnabled
    public static String getTimeZoneSidKey(){
        
    	String TimeZoneSidKey =  [SELECT Id, TimeZoneSidKey FROM Organization].TimeZoneSidKey;
        
    	return TimeZoneSidKey;
    }
}