@isTest
public class TeleMeetTrigger_Test {
    @isTest
    public static void testBeforeInsert(){
        Account testAccount = new Account();
        testAccount.Name='Test Account' ;
        insert testAccount;
        
        Contact cont = new Contact();
        cont.FirstName='Test';
        cont.LastName='Test';
        cont.Accountid= testAccount.id;
        insert cont;
        
        Contact cont2 = new Contact();
        cont2.FirstName='Test';
        cont2.LastName='Test2';
        cont2.Accountid= testAccount.id;
        cont2.Email = 'test2@test.com';
        insert cont2;
        
        Contact cont3 = new Contact();
        cont3.FirstName='Test';
        cont3.LastName='Test3';
        cont3.Accountid= testAccount.id;
        cont3.Email = 'test3@test.com';
        insert cont3;
        
        Datetime timeNow = System.now();
        DateTime earlier = timeNow.addMinutes(5);
        DateTime fullHour = timeNow.addMinutes( timeNow.minute() ).addSeconds( timeNow.second() );
        
        Contact_Availability__c conAva = new Contact_Availability__c();
        conAva.contact__c = cont.id;
        conAva.Start_Date_Time__c = timeNow;
        conAva.End_Date_Time__c = fullHour;
        Insert conAva;
        
        Practice_Management__c pracManagement = new Practice_Management__c();
        Insert pracManagement;
        
        Id recType = Schema.SObjectType.TeleMeet__c.getRecordTypeInfosByName().get('Default').getRecordTypeId();
        
        TeleMeet__c telemeet = new TeleMeet__c();
        telemeet.ContactAvailabilityId__c = conAva.Id;
        teleMeet.RecordTypeId = recType;
       	teleMeet.Practice_Management__c = pracManagement.Id;
        teleMeet.Resource_Contact__c = cont2.Id;
        teleMeet.Type__c = 'Navigator Connect';
        teleMeet.Contact__c = cont3.Id;
        teleMeet.Description__c = 'test only';
        teleMeet.Subject__c = 'test only';
        Insert telemeet;
        
        telemeet.Status__c = 'Draft';
        Update telemeet;
        
        telemeet.Status__c = 'Scheduled';
        Update telemeet;
    }
}