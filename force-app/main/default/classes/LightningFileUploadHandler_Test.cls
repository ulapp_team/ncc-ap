/**
 * @description LightningFileUploadHandler Test Class
 *           05/06/2021 - Kyzer Buhay - Initial Creation
 **/
@IsTest
public class LightningFileUploadHandler_Test {
  @TestSetup
  static void createData() {
    Profile profileId = [
      SELECT Id
      FROM Profile
      WHERE Name = 'Compass Profile'
      LIMIT 1
    ];

    User usr = new User(
      LastName = 'Site Guest User',
      FirstName = 'Compass',
      Alias = 'guest',
      Email = 'compasssiteguetuser@test.com',
      Username = 'compassGuestUser@test.com',
      ProfileId = profileId.id,
      TimeZoneSidKey = 'GMT',
      LanguageLocaleKey = 'en_US',
      EmailEncodingKey = 'UTF-8',
      LocaleSidKey = 'en_US'
    );

    insert usr;
    Account newAccount = new Account();
    newAccount.Name = 'John Doe\'s Inc.';

    Id accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName()
      .get('Customer')
      .getRecordTypeId();
    newAccount.RecordTypeId = accRecordTypeId;
    insert newAccount;

    Contact newContact = new Contact();
    newContact.Firstname = 'John';
    newContact.Lastname = 'Doe';
    newContact.Email = 'john.doe@test.com';
    newContact.AccountId = newAccount.Id;
    insert newContact;
  }

  @IsTest
  static void testUploadImage_AsGuestUser() {
    User u = [
      SELECT Id
      FROM User
      WHERE Email = 'compasssiteguetuser@test.com'
      LIMIT 1
    ];
    Test.startTest();

    ContentVersion contentVersion = new ContentVersion(
      Title = 'Penguins',
      PathOnClient = 'Penguins.jpg',
      VersionData = Blob.valueOf('Test Content'),
      IsMajorVersion = true
    );
    insert contentVersion;

    Contact contactRec = [
      SELECT Id, Email
      FROM Contact
      WHERE Email = 'john.doe@test.com'
      LIMIT 1
    ];
    LightningFileUploadHandler.ContactProfileWrapper wrapper = LightningFileUploadHandler.updatePicturePath(
      contactRec.Id,
      contentVersion.Id
    );

    Contact updatedCon = [
      SELECT Id, Profile_Picture_URL__c
      FROM Contact
      WHERE Email = 'john.doe@test.com'
      LIMIT 1
    ];
    System.assert(!wrapper.hasErrors);
    System.assertEquals(wrapper.profileURL, updatedCon.Profile_Picture_URL__c);
    Test.stopTest();
  }

  @IsTest
  static void testRemoveImage_AsGuestUser() {
    User u = [
      SELECT Id
      FROM User
      WHERE Email = 'compasssiteguetuser@test.com'
      LIMIT 1
    ];
    ContentVersion contentVersion = new ContentVersion(
      Title = 'Penguins',
      PathOnClient = 'Penguins.jpg',
      VersionData = Blob.valueOf('Test Content'),
      IsMajorVersion = true
    );
    insert contentVersion;
    Contact contactRec = [
      SELECT Id, Email, Profile_Picture_URL__c
      FROM Contact
      WHERE Email = 'john.doe@test.com'
      LIMIT 1
    ];
    LightningFileUploadHandler.updatePicturePath(
      contactRec.Id,
      contentVersion.Id
    );

    Test.startTest();
    Boolean removed = LightningFileUploadHandler.removeAndDeleteImageURL(
      contactRec.Id,
      '/Compass/sfc/servlet.shepherd/version/renditionDownload?rendition=ORIGINAL_JPG&versionId=' +
      contentVersion.Id +
      '&operationContext=CHATTER'
    );
    System.assert(removed);
    Test.stopTest();
  }
}