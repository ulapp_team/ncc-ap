/*******************************************************************************************
  * @name: EventConfirmationResponseController
  * @author: Paolo Quiambao
  * @created: 26-08-2021
  * @description: Controller for Confirm/Delete upon clicking the button from the Event Confirmation Template Email
  *
  * Changes (version)
  * -------------------------------------------------------------------------------------------
  *             No.   Date(dd-mm-yyy)   Author                Description
  *             ----  ---------------   --------------------  ---------------------------------
  * @version    1.0   26-08-2021        Paolo Quiambao        Initial Creation
  * 			2.0	  30-04-2022		Paolo Quiambao		  Enhance logic to accept multiple Session Participant Ids
  *********************************************************************************************/
public class EventConfirmationResponseController {
    public String sessionParticipantIds {get;set;} // Updated by PaoloQuiambao [ccneve1216dv] April302022
    public String response {get;set;}
    public List<String> sessionParticipantIdList {get;set;} // Updated by PaoloQuiambao [ccneve1216dv] April302022
    
    public void updateSessionParticipantStatus() {
        sessionParticipantIds = ApexPages.currentPage().getParameters().get('id');
        sessionParticipantIdList = sessionParticipantIds.split(' ');
        response = ApexPages.currentPage().getParameters().get('response');
        List<Session_Participant__c> sessionParticipantListToUpdate = new List<Session_Participant__c>(); // Updated by PaoloQuiambao [ccneve1216dv] April302022
        
        if(!sessionParticipantIdList.isEmpty()) {
            try {
                // Updated by PaoloQuiambao [ccneve1216dv] April302022
                for(Session_Participant__c sp : [SELECT Id, Status__c FROM Session_Participant__c WHERE Id IN :sessionParticipantIdList]) {
                    sp.Status__c = response;
                    sessionParticipantListToUpdate.add(sp);
                }
            }
            catch(Exception err) {
                System.Debug('TRY-CATCH-Exception-->' + err);
            }
            
            try {
                if(!sessionParticipantListToUpdate.isEmpty()) {
                    update sessionParticipantListToUpdate;
                }
            }
            catch(Exception err) {
                System.Debug('TRY-CATCH-Exception-->' + err);
            }
            
        }
    }
}