public class VirtuVisitController {
   //Set Navigation Menu
    @AuraEnabled        
    public static List<Connect_Navigation__c> getNavigationMenu(){
        List<Connect_Navigation__c> menu = [SELECT Name, Menu_URL__c FROM Connect_Navigation__c WHERE Active__c =: TRUE ORDER BY Sequence__c]; 
        
        return menu;
    }
    //Get SFTimezone via Metadata
    @AuraEnabled        
    public static Map<String, String> getSFTimezone(){
        Map<String, String> SFTimezoneMap = new Map<String, String>();
        List<SFTimezone__mdt> sft = [SELECT DeveloperName, Timezone_Name__c FROM SFTimezone__mdt ORDER BY DeveloperName]; 
        for(SFTimezone__mdt s: sft){
            SFTimezoneMap.put(s.DeveloperName, s.Timezone_Name__c);
        }
        
        return SFTimezoneMap;
    }
    
    
   /*@AuraEnabled        
   public static void createQuestionnaire(String questionnaireName, String questionnaireDescription, String question, String questionType, String options, String questionList){
       try{
           Question_Tab_Header__c qth = new Question_Tab_Header__c();
           qth.Name = questionnaireName;
           qth.Description__c = questionnaireDescription;
           insert qth;
           createQuestions(String.valueOf(qth.Id), question, questionType, options, questionList);
       }
        catch(Exception ex){
            system.debug('Apex Class Error: VirtuVisitController.createQuestionnaire >>> '+ex.getMessage());
        }
    }
    
    @Future
    @AuraEnabled        
    public static void createQuestions(String qth, String question, String questionType, String options, String questionList){
        
        try{
            List<QuestionAndAnswerWrapper> qList = new List<QuestionAndAnswerWrapper>();
            qList = (List<QuestionAndAnswerWrapper>)JSON.deserialize(questionList, List<QuestionAndAnswerWrapper>.class);
            
            List<Intake_Form__c> iqList = new List<Intake_Form__c>();
            for(QuestionAndAnswerWrapper q: qList){ 
                Intake_Form__c iq = new Intake_Form__c();
                iq.Description__c = q.Question;
                iq.Question_Type__c = q.QuestionType;
                iq.Options__c = q.Options;
                iq.Question_Tab_Header__c = qth;
                iqList.add(iq);
            }
            if(!iqList.isEmpty()){
                insert iqList;
            }
        }
        catch(Exception ex){
            system.debug('Apex Class Error: VirtuVisitController.createQuestions >>> '+ex.getMessage());
        }
        
       
    }*/
    
    @AuraEnabled        
    public static Map<String, String> getTimeZoneValuesFromField(){
        Map<String, String>  pickListValuesMap = new Map<String, String>(); 
        Schema.DescribeFieldResult fieldResult = Contact_Availability__c.Time_Zone__c.getDescribe();
            
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            pickListValuesMap.put(pickListVal.getLabel(), pickListVal.getValue());
        }
        system.debug('pickListValuesMap'+pickListValuesMap);
        return pickListValuesMap;
    }
    
    
    @AuraEnabled
    public static List<Service__c> getAllServicesRecord(String conId){
        List<Service__c> service = [Select Id, Name, Description__c, Display_Practice__c, Display_at_Home__c From Service__c Where Active__c = TRUE Order By CreatedDate];
                
        return service;
    }
    @AuraEnabled
    public static Service__c getServiceRecord(String serviceId){
        Service__c service = [Select Id, Name, Description__c, Reason__c, Display_Practice__c, Display_at_Home__c From Service__c Where Id =: serviceId];
                
        return service;
    }
    
    @AuraEnabled
    public static List<Practice_Management__c> getAllPracticeManagementRecord(String conId){
        List<Practice_Management__c> practice = [Select Id, Name, Description__c From Practice_Management__c Where Active__c = TRUE Order By CreatedDate];
                
        return practice;
    }
    
    @AuraEnabled
    public static List<Service__c> getPracticeServicesRecord(String practiceId){
        List<Service__c> services = [Select Id, Name, Description__c, Reason__c From Service__c Where Practice_Management__c =: practiceId AND Active__c = TRUE  Order By CreatedDate];
                
        return services;
    }
    
    
    @AuraEnabled
    public static List<Service_Practice__c> getServicePracticesRecord(String serviceId){
        List<Service_Practice__c> servicePractices = [Select Id, Name, Description__c, Service__c, Service__r.Name, Practice_Management__c, Practice_Management__r.Name From Service_Practice__c Where Service__c =: serviceId];
                
        return servicePractices;
    }
    
    
    @AuraEnabled
    public static List<Practice_Management_Group__c> getPracticeDoctorsRecord(String practiceId){
        return [Select Id, Name, Role__c, Contact__c, Contact__r.Name, Contact__r.Phone, Contact__r.Email, Contact__r.Account.Name, Practice_Management__c, Contact__r.Profile_Picture_URL__c From Practice_Management_Group__c Where Practice_Management__c =: practiceId and Role__c = 'Doctor'];
    }
    
    @AuraEnabled
    public static List<Practice_Management_Group__c> getAllDoctors(String serviceId){
        List<Service_Practice__c> sp = [Select Practice_Management__c, Service__c From Service_Practice__c Where Service__c =: serviceId];
        Set<Id> PMMap = new Set<Id>();
        for(Service_Practice__c s: sp){
            PMMap.add(s.Practice_Management__c);
        }
        Set<Id> PMRMap = new Set<Id>();
        List<Practice_Management_Group__c> PMRole = [Select Id, Name, Role__c, Contact__c, Contact__r.Name, Contact__r.Title, Contact__r.Phone, Contact__r.Email, Contact__r.Account.Name, Contact__r.Profile_Picture_URL__c,
                                                     Practice_Management__c, Practice_Management__r.Name 
                                                     From Practice_Management_Group__c Where Practice_Management__c IN: PMMap and Role__c = 'Doctor' Order By Contact__r.Name,  Practice_Management__r.Name];
    	
        /*for(Practice_Management_Group__c s: PMRole){
            PMRMap.add(s.Contact__c);
        }
        List<Contact> conList = [Select Id, Name, Account.Name, Email, Phone, Title, Profile_Picture_URL__c From Contact Where Id IN: PMRMap];
        */
        return PMRole;
    }
    @AuraEnabled
    public static Contact getDoctorRecord(String doctorId){
        return [Select Id, Name, Title, Profile_Picture_URL__c, Availability_Time_Zone__c From Contact Where Id =: doctorId LIMIT 1];
    }
    
    @AuraEnabled
    public static List<Telemeet__c> getMyAppointments(String contactId){
        return [Select Id, CreatedDate, Contact__c, Start_Date_Time__c, End_Date_Time__c, Time_Zone__c, Status__c, Resource_Contact__r.Name, Meeting_URL2__c, VirtuVisit_Link__c From Telemeet__c Where Contact__c =: contactId];
    }
    
    
    @AuraEnabled
    public static String createContactRecord(String firstName, String lastName, String email, String password){
        String result = '';
        try{
            List<Contact> conList = [Select Name From Contact Where Email =: email];
            List<Account> accList = [Select Id From Account LIMIT 1]; //should be checked because required for sharing rule
            if(conList.isEmpty()){
                Contact con = new Contact();
                con.FirstName = firstName;
                con.LastName = lastName;
                con.Email = email;
                con.Password__c = password;
                con.AccountId = accList[0].Id;
                insert con;
                result = 'Succesfully registered.';
            }
            else{
                result = 'Email is already existing.';
            }
        }
        catch(Exception ex){
            result = ex.getMessage();
        }
        return result;
    }
    
    @AuraEnabled
    public static List<Intake_Form__c> getIntakeFormRecords(String conId){
        List<Intake_Form__c> intQue = [SELECT Id, Name, Description__c, Question_Type__c, Options__c FROM Intake_Form__c order by Createddate];
        return intQue;
    }
    
    @AuraEnabled
    public static Map<String, List<Intake_Form__c>> getIntakeFormRecordsMap(String conId){
        Map<String, List<Intake_Form__c>> intQueMap = new Map<String, List<Intake_Form__c>>();
        List<Intake_Form__c> intQueList = [SELECT Id, Name, Description__c, Question_Type__c, Options__c, Checkin_Type__c FROM Intake_Form__c where Checkin_Type__c!=null order by Createddate];
        for(Intake_Form__c i: intQueList){
            if(intQueMap.containsKey(i.Checkin_Type__c)){
                intQueMap.get(i.Checkin_Type__c).add(i);
            }
            else{
                intQueMap.put(i.Checkin_Type__c, new List<Intake_Form__c> {i} );
            }
        }
        return intQueMap;
    }
    
    ///clone
    @AuraEnabled
    public static Map<String, List<Intake_Form__c>> getIntakeFormRecordsMap2(String conId){
        
        List<Intake_Form_Tab__c> iftList = [Select Id, Name, Description__c From Intake_Form_Tab__c order by Step__c ASC];
        
        Map<String, List<Intake_Form__c>> intQueMap = new Map<String, List<Intake_Form__c>>();
        List<Intake_Form__c> intQueList = [SELECT Id, Name, Description__c, Question_Type__c, Options__c, Step__c, Required__c, Intake_Form_Tab__r.Name, Intake_Form_Tab__r.Description__c FROM Intake_Form__c where Intake_Form_Tab__c IN:iftList AND Intake_Form_Tab__c != null Order by Intake_Form_Tab__r.Step__c, Step__c];
        String description = '';
        for(Intake_Form__c i: intQueList){
            description = (i.Intake_Form_Tab__r.Description__c != null ? i.Intake_Form_Tab__r.Description__c : '');
            if(intQueMap.containsKey(i.Intake_Form_Tab__r.Name+'*****'+description)){
                intQueMap.get(i.Intake_Form_Tab__r.Name+'*****'+description).add(i);
            }
            else{
                intQueMap.put(i.Intake_Form_Tab__r.Name+'*****'+description, new List<Intake_Form__c> {i} );
            }
        }
        return intQueMap;
    }
    
    @AuraEnabled
    public static List<Intake_Form_Tab__c> getIntakeFormTabs(String conId){
        List<Intake_Form__c> ifList = [Select Intake_Form_Tab__c From Intake_Form__c Where Intake_Form_Tab__c != null];
        Set<Id> iftIds = new Set<Id>();
        for(Intake_Form__c i : ifList){
            iftIds.add(i.Intake_Form_Tab__c);
        }
        return [Select Id, Name, Description__c, Step__c From Intake_Form_Tab__c Where Id IN :iftIds Order by Step__c ASC];
    }
        
    
    
    @AuraEnabled
    public static String loginContact(String username, String password){
        String result;
        List<Contact> conList = [Select Id, Name, FirstName From Contact Where Email =: username AND Password__c =: password];
        
        if(conList.isEmpty()){
            result = 'Invalid Credentials';
        }
        else{
            result = 'success-'+conList[0].Id;
        }
        return result;
    }
    
    @AuraEnabled
    public static Contact getContactRecord(String conId){
        return [Select Id, Name, FirstName, Account.Name, Profile_Picture_URL__c From Contact Where Id=: conId];
    }
    
    
    
    @AuraEnabled
    public static void createEncounter(String conId, String serviceId, String reason, String doctorId, String result, String contactAvailabilityId, String practiceManagementId, String selectedTimeZone){
        
        if (String.isBlank(conId) || String.isBlank(result)) return;
        
        //Create Encounter Record 
        Encounter__c encounter = new Encounter__c();
        encounter.Name = 'Temporary Name';
        encounter.Contact__c = conId;
        encounter.Encounter_Date__c = Date.Today();
        encounter.Type__c = 'VirtuVisit';
        encounter.Reason__c = reason;
        encounter.Resource__c = doctorId;
        encounter.Service__c = serviceId;
        encounter.ContactAvailabilityId__c = contactAvailabilityId;
        encounter.Practice_Management__c = practiceManagementId;
        encounter.Time_Zone__c = selectedTimeZone;
        
        try{
            //system.assertEquals(encounter, null);
            insert encounter;
            saveIntakeForm(conId, serviceId, reason, doctorId, result, String.valueOf(encounter.Id), contactAvailabilityId);
        } catch(DmlException e){
            throw new AuraHandledException(e.getMessage());
        }
    }
    
    @Future
    @AuraEnabled
    public static void saveIntakeForm(String conId, String serviceId, String reason, String doctorId, String result, String encounterId, String contactAvailabilityId){
        
        // Create result records, link these to the response
        Map<String, UserAnswerWrapper> questionIDToUserAnswerMap = new Map<String, UserAnswerWrapper>();
        questionIDToUserAnswerMap = (Map<String, UserAnswerWrapper>)JSON.deserialize(result, Map<String, UserAnswerWrapper>.class);
        system.debug('questionIDToUserAnswerMap: '+questionIDToUserAnswerMap);
        List<Intake_Answer__c> answersList = new List<Intake_Answer__c>();
        String answerList;
        for (String questionId : questionIDToUserAnswerMap.keySet()){
            UserAnswerWrapper uaw = questionIDToUserAnswerMap.get(questionId);
            Intake_Answer__c sa = new Intake_Answer__c();
            sa.Intake_Form__c = questionId;
            sa.Question__c = uaw.Question;
            sa.Encounter__c = encounterId;
            //system.assertEquals(uaw.AnswerList, null);
            if(uaw.AnswerList != null){
                answerList = String.valueOf(uaw.AnswerList).replaceAll('\\(', '');
                answerList = answerList.replaceAll(', ', '\n');
                answerList = answerList.replaceAll('\\)', '');
            }
            sa.Answer__c = (uaw.Answer != null ? uaw.Answer : answerList);
            sa.Contact__c = conId;
            answersList.add(sa);
        }
        
        // DML commit
        try{
            insert answersList;
        } catch(DmlException e){
            throw new AuraHandledException(e.getMessage());
        } 
    }
    
    
    
    // User answer wrapper class
    public class QuestionAndAnswerWrapper{
        public String Question;
        public String Options;
        public String QuestionType;
    }
    
    // User answer wrapper class
    public class UserAnswerWrapper{
        public String Question;
        public String Answer;
        public List<String> AnswerList;
    }
    
    
    //Contact Availability
    @AuraEnabled
    public static List<Contact_Availability__c> getContactAvailability(String conId){
        List<Contact_Availability__c> conAva = [Select Id, Contact__c, Contact__r.Availability_Time_Zone__c, Contact__r.Organization_Time_Zone__c, Time_Zone__c, Start_Date_Time__c, End_Date_Time__c, Status__c From Contact_Availability__c Where Contact__c =: conId Order By Start_Date_Time__c];
        return conAva;
    }
    
    //Contact Availability
    //@AuraEnabled
    /*public static List<Contact_Availability__c> getContactAvailability2(String conId){
        List<Contact_Availability__c> conAva = [Select Id, Contact__c, Contact__r.Availability_Time_Zone__c, Time_Zone__c, Start_Date_Time__c, End_Date_Time__c, Status__c From Contact_Availability__c Where Contact__c =: conId Order By Start_Date_Time__c];
        List<Contact_Availability__c> conAvaConverted = new List<Contact_Availability__c>();
        Map<String, String> SFTimezone = new Map<String, String>();
        for (SFTimezone__mdt s: [Select Id, MasterLabel, DeveloperName, Timezone_Name__c From SFTimezone__mdt]){
            SFTimezone.put(s.MasterLabel, s.Timezone_Name__c);
        }
        
        String TimeZoneSidKey =  [SELECT Id, TimeZoneSidKey FROM Organization].TimeZoneSidKey;
        
        Timezone tz;
        String timeZoneUpdate = '';
        
        for(Contact_Availability__c c : conAva){
            DateTime startDT = c.Start_Date_Time__c;
            DateTime endDT = c.End_Date_Time__c;
            
            system.debug('startDT old>>>'+startDT);
            system.debug('endDT old>>>'+endDT);
            
            String startDTGMT = startDT.format('MM/dd/yyyy/HH/mm/ss', TimeZoneSidKey);
            String endDTGMT = endDT.format('MM/dd/yyyy/HH/mm/ss', TimeZoneSidKey);
            
            system.debug('startDTGMT >>>'+startDTGMT );
            system.debug('endDTGMT>>>'+endDTGMT);
            
            List<String> startDTtz = startDTGMT.split('/');
            List<String> endDTtz = endDTGMT.split('/');
            
            DateTime startDTNew = DateTime.newInstanceGMT(Integer.valueOf(startDTtz[2]), Integer.valueOf(startDTtz[0]), Integer.valueOf(startDTtz[1]),  Integer.valueOf(startDTtz[3]), 
                                                          Integer.valueOf(startDTtz[4]), Integer.valueOf(startDTtz[5]));
            DateTime endDTNew = DateTime.newInstanceGMT(Integer.valueOf(endDTtz[2]), Integer.valueOf(endDTtz[0]), Integer.valueOf(endDTtz[1]),  Integer.valueOf(endDTtz[3]), 
                                                        Integer.valueOf(endDTtz[4]), Integer.valueOf(endDTtz[5]));
            
            c.Start_Date_Time__c = startDTNew;
            c.End_Date_Time__c = endDTNew;
            conAvaConverted.add(c);
        }
        
        return conAvaConverted;
    }*/
    
    @AuraEnabled
    public static String getTimeZoneSidKey(){
        String TimeZoneSidKey =  [SELECT Id, TimeZoneSidKey FROM Organization].TimeZoneSidKey;
        return TimeZoneSidKey;
    }
    
}