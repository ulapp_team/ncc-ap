public class EncounterTriggerHandler {

    public static void onBeforeInsert(List < Encounter__c > newEncounterList, Map < Id, Encounter__c > newEncounterMap, List < Encounter__c > oldEncounterList, Map < Id, Encounter__c > oldEncounterMap) {

    }

    public static void onAfterInsert(List < Encounter__c > newEncounterList, Map < Id, Encounter__c > newEncounterMap, List < Encounter__c > oldEncounterList, Map < Id, Encounter__c > oldEncounterMap) {
        List < TeleMeet__c > connectList = new List < TeleMeet__c > ();
        List < Encounter__c > encounterList = [Select Id, Encounter_Date__c, Reason__c, Type__c, Resource__c, Contact__c, Contact__r.FirstName, Contact__r.LastName, ContactAvailabilityId__c, Practice_Management__c, Time_Zone__c, Internal_Notes__c From Encounter__c WHERE Id IN: newEncounterList];

        Map < Id, Id > conAvaEncounterMap = new Map < Id, Id > ();
        List < Id > conAvaIdList = new List < Id > ();
        for (Encounter__c e: encounterList) {
            conAvaIdList.add(e.ContactAvailabilityId__c);
            conAvaEncounterMap.put(e.ContactAvailabilityId__c, e.Id);
        }

        List < Contact_Availability__c > conAvaList = [Select Id, Start_Date_Time__c, End_Date_Time__c, Time_zone__c From Contact_Availability__c Where Id IN: conAvaIdList];

        Map < Id, Contact_Availability__c > encounterConAvaRecMap = new Map < Id, Contact_Availability__c > ();
        for (Contact_Availability__c c: conAvaList) {
            encounterConAvaRecMap.put(c.Id, c);
            //c.TeleMeet__c = (conAvaEncounterMap.containsKey(c.Id) ? conAvaEncounterMap.get(c.Id): null);
        }

        //Timezone
        Map < String, String > SFTimezone = new Map < String, String > ();
        for (SFTimezone__mdt s: [Select Id, MasterLabel, DeveloperName, Timezone_Name__c From SFTimezone__mdt]) {
            SFTimezone.put(s.MasterLabel, s.Timezone_Name__c);
        }

        String TimeZoneSidKey = [SELECT Id, TimeZoneSidKey FROM Organization].TimeZoneSidKey;
        Timezone tz;
        String timeZoneUpdate = '';


        for (Encounter__c e: encounterList) {
            if (e.Contact__c != null) {
                e.Name = (String.valueOf(e.Encounter_Date__c)).LEFT(10) + ' ' + e.Contact__r.FirstName + ' ' + e.Contact__r.LastName + ' ' + e.Type__c;
            } else {
                e.Name = (String.valueOf(e.Encounter_Date__c)).LEFT(10) + ' ' + e.Type__c;
            }

            TeleMeet__c tel = new TeleMeet__c();
            if (e.Type__c == 'VirtuVisit') {
                tel.Contact__c = e.Contact__c;
                tel.Resource_Contact__c = e.Resource__c;
                tel.Encounter__c = e.Id;
                tel.Status__c = 'Draft';
                tel.Target_Appointment_Date__c = e.Encounter_Date__c;
                //tel.Start_Date_Time__c = e.Encounter_Date__c;
                //tel.End_Date_Time__c = e.Encounter_Date__c;
                //tel.Subject__c = e.Reason__c;
                //tel.Description__c = e.Reason__c;
                //for testing
                tel.Subject__c = e.Internal_Notes__c;
                tel.Description__c = e.Internal_Notes__c;
                
                tel.Type__c = 'Telehealth Meet';
                tel.Practice_Management__c = e.Practice_Management__c;
                tel.Show_in_Lobby__c = TRUE;
                tel.ContactAvailabilityId__c = e.ContactAvailabilityId__c;

            }
            if (e.Type__c == 'VirtuVisit') {
                tel.Type__c = 'VirtuVisit';
                tel.Status__c = 'Scheduled';
                tel.ContactAvailabilityId__c = e.ContactAvailabilityId__c;
                
                //Timezone Convert
                //Convert Based on the Company Timezone
                DateTime startDT = (encounterConAvaRecMap.containsKey(e.ContactAvailabilityId__c) ? encounterConAvaRecMap.get(e.ContactAvailabilityId__c).Start_Date_Time__c : null);
                DateTime endDT = (encounterConAvaRecMap.containsKey(e.ContactAvailabilityId__c) ? encounterConAvaRecMap.get(e.ContactAvailabilityId__c).End_Date_Time__c : null);

                String startDTGMT = startDT.format('MM/dd/yyyy/HH/mm/ss', TimeZoneSidKey);
                String endDTGMT = endDT.format('MM/dd/yyyy/HH/mm/ss', TimeZoneSidKey);

                system.debug('startDTGMT >>>' + startDTGMT);
                system.debug('endDTGMT>>>' + endDTGMT);

                List < String > startDTtz1 = startDTGMT.split('/');
                List < String > endDTtz1 = endDTGMT.split('/');

                DateTime startDTNewComp = DateTime.newInstanceGMT(Integer.valueOf(startDTtz1[2]), Integer.valueOf(startDTtz1[0]), Integer.valueOf(startDTtz1[1]), Integer.valueOf(startDTtz1[3]),
                    Integer.valueOf(startDTtz1[4]), Integer.valueOf(startDTtz1[5]));
                DateTime endDTNewRecComp = DateTime.newInstanceGMT(Integer.valueOf(endDTtz1[2]), Integer.valueOf(endDTtz1[0]), Integer.valueOf(endDTtz1[1]), Integer.valueOf(endDTtz1[3]),
                    Integer.valueOf(endDTtz1[4]), Integer.valueOf(endDTtz1[5]));

                //Convert based on the record timezone
                timeZoneUpdate = SFTimezone.get((encounterConAvaRecMap.containsKey(e.ContactAvailabilityId__c) ? encounterConAvaRecMap.get(e.ContactAvailabilityId__c).Time_Zone__c : null));
                String startDTGMTRec = startDT.format('MM/dd/yyyy/HH/mm/ss', timeZoneUpdate);
                String endDTGMTRec = endDT.format('MM/dd/yyyy/HH/mm/ss', timeZoneUpdate);

                List < String > startDTtz = startDTGMTRec.split('/');
                List < String > endDTtz = endDTGMTRec.split('/');

                DateTime startDTNewRec = DateTime.newInstanceGMT(Integer.valueOf(startDTtz[2]), Integer.valueOf(startDTtz[0]), Integer.valueOf(startDTtz[1]), Integer.valueOf(startDTtz[3]),
                    Integer.valueOf(startDTtz[4]), Integer.valueOf(startDTtz[5]));
                DateTime endDTNewRec = DateTime.newInstanceGMT(Integer.valueOf(endDTtz[2]), Integer.valueOf(endDTtz[0]), Integer.valueOf(endDTtz[1]), Integer.valueOf(endDTtz[3]),
                    Integer.valueOf(endDTtz[4]), Integer.valueOf(endDTtz[5]));

                //Convert based on the selected timezone
                timeZoneUpdate = SFTimezone.get(e.Time_Zone__c);
                String startDTGMTSel = startDT.format('MM/dd/yyyy/HH/mm/ss', timeZoneUpdate);
                String endDTGMTSel = endDT.format('MM/dd/yyyy/HH/mm/ss', timeZoneUpdate);

                List < String > startDTtz2 = startDTGMTSel.split('/');
                List < String > endDTtz2 = endDTGMTSel.split('/');

                DateTime startDTNewSel = DateTime.newInstanceGMT(Integer.valueOf(startDTtz2[2]), Integer.valueOf(startDTtz2[0]), Integer.valueOf(startDTtz2[1]), Integer.valueOf(startDTtz2[3]),
                    Integer.valueOf(startDTtz2[4]), Integer.valueOf(startDTtz2[5]));
                DateTime endDTNewSel = DateTime.newInstanceGMT(Integer.valueOf(endDTtz2[2]), Integer.valueOf(endDTtz2[0]), Integer.valueOf(endDTtz2[1]), Integer.valueOf(endDTtz2[3]),
                    Integer.valueOf(endDTtz2[4]), Integer.valueOf(endDTtz2[5]));

                //StartDate
                Long dt1Long = startDTNewRec.getTime();
                Long dt2Long = startDTNewSel.getTime();
                Long milliseconds = dt2Long - dt1Long;
                Long seconds = milliseconds / 1000;
                Long minutes = seconds / 60;
                Long hours = minutes / 60;
                Long days = hours / 24;

                DateTime finalStartDate = startDTNewComp.addHours(Integer.valueOf(hours));


                //EndDate
                Long dt1Long2 = endDTNewRec.getTime();
                Long dt2Long2 = endDTNewSel.getTime();
                Long milliseconds2 = dt2Long2 - dt1Long2;
                Long seconds2 = milliseconds2 / 1000;
                Long minutes2 = seconds2 / 60;
                Long hours2 = minutes2 / 60;
                Long days2 = hours2 / 24;

                DateTime finalEndDate = endDTNewRecComp.addHours(Integer.valueOf(hours2));

                //Convert Based on the Company Timezone
                DateTime startDTFinal = finalStartDate;
                DateTime endDTFinal = finalEndDate;

                String startDTGMTFinal = startDTFinal.format('MM/dd/yyyy/HH/mm/ss', TimeZoneSidKey);
                String endDTGMTFinal = endDTFinal.format('MM/dd/yyyy/HH/mm/ss', TimeZoneSidKey);


                List < String > startDTtzFinal = startDTGMTFinal.split('/');
                List < String > endDTtzFinal = endDTGMTFinal.split('/');

                DateTime startDTNewFinal = DateTime.newInstanceGMT(Integer.valueOf(startDTtzFinal[2]), Integer.valueOf(startDTtzFinal[0]), Integer.valueOf(startDTtzFinal[1]), Integer.valueOf(startDTtzFinal[3]),
                    Integer.valueOf(startDTtzFinal[4]), Integer.valueOf(startDTtzFinal[5]));
                DateTime endDTNewRecFinal = DateTime.newInstanceGMT(Integer.valueOf(endDTtzFinal[2]), Integer.valueOf(endDTtzFinal[0]), Integer.valueOf(endDTtzFinal[1]), Integer.valueOf(endDTtzFinal[3]),
                    Integer.valueOf(endDTtzFinal[4]), Integer.valueOf(endDTtzFinal[5]));

                tel.Start_Date_Time__c = (encounterConAvaRecMap.containsKey(e.ContactAvailabilityId__c) ? encounterConAvaRecMap.get(e.ContactAvailabilityId__c).Start_Date_Time__c : null);
                tel.End_Date_Time__c = (encounterConAvaRecMap.containsKey(e.ContactAvailabilityId__c) ? encounterConAvaRecMap.get(e.ContactAvailabilityId__c).End_Date_Time__c : null);
                tel.Time_Zone__c = (encounterConAvaRecMap.containsKey(e.ContactAvailabilityId__c) ? encounterConAvaRecMap.get(e.ContactAvailabilityId__c).Time_zone__c : null);
                connectList.add(tel);
            }
        }
        if (!connectList.isEmpty()) {
            insert connectList;
        }
        
        update encounterList;

        //update Connect field in Contact Availability record
        List < Telemeet__c > connectListRef = [Select Id, Encounter__c From Telemeet__c Where Encounter__c IN: encounterList];
        Map < Id, Id > connectConAvaMap = new Map < Id, Id > ();
        for (Telemeet__c t: connectListRef) {
            connectConAvaMap.put(t.Encounter__c, t.Id);
        }
        for (Contact_Availability__c c: conAvaList) {
            c.TeleMeet__c = (conAvaEncounterMap.containsKey(c.Id) ? (connectConAvaMap.containsKey(conAvaEncounterMap.get(c.Id)) ? connectConAvaMap.get(conAvaEncounterMap.get(c.Id)) : null) : null);
        }
        update conAvaList;

    }
}