Project : COMPASS
Deployment Structure : SFDX

Repository 1 : NCC-AP

master - connected to production org (htang@ulapp.co.gpo)
DEV - connected to dev org (htang@ncc.com.pdo)

Branching
feature/<ticket-number> - use this branch name when changes occurs on dev org
hotfix/<ticket-number> - use this branch name when changes occurs on production org
release/<date-today> - use this branch name to merge changes for dev and production org (to be cloned from master)

Deployment Process
1. create pull request from feature/* branch to DEV branch
2. merge the pull request once validation is success
3. create pull request from hotfix/* branch to master branch
4. merge the pull request once validation is success
5. create a branch release/date-today to be cloned from master
6. pull the DEV branch into the release/date-today branch
7. resolve conflicts using 'resolve from theirs' option
8. push the release branch
9. create pull request from release/<date-today> to master and set Nicole De Guzman as approver
10. once approved, merge the pull request
